import json
import os.path
from django.template.response import TemplateResponse

import time
from datetime import date
from datetime import timedelta
from django.http import HttpResponse
from django.utils.translation import activate as activate_translations, get_language_from_path, ugettext as _
from wagtail.core.models import Page
from wagtail.core.fields import RichTextField
from wagtail.admin.edit_handlers import FieldPanel
from wagtail.contrib.routable_page.models import RoutablePageMixin, route
from django.shortcuts import render
from mapo.settings.base import LANGUAGE_CODE, BASE_URL, PROJECT_DIR
from maplisting.categories import CATEGORIES, CATEGORIES_PV
from maplisting.utils import (
    get_preferred_langs, get_url_from_point, transform_embedded_videos,
    get_feature, get_social_currency_points, show_social_currency_point,
    get_social_currency_point_short_details, get_ecovillage_points, show_gen_ecovillage_point,
    show_fc_local_node_point, get_fc_local_nodes_points, get_sobirania_points, show_sobirania_point
)
from maplisting.models.map_listing_translation_page import MapListingTranslationPage


class MapListingIndexPage(RoutablePageMixin, Page):
    social_currency_points = None
    social_currency_data = None
    ecovillage_points = None
    fc_local_nodes_points = None
    fc_local_nodes_data = None
    ecovillage_data = None
    intro = RichTextField(blank=True)

    content_panels = Page.content_panels + [
        FieldPanel('intro', classname="full")
    ]

    subpage_types = ['MapListingPage']

    @route(r'^getCategoriesPV/lang/([a-z][a-z])/$')
    def show_categories_pv(self, request, lang=None):
        sorted_slugs = []
        categories_translated = {}

        for slug, cat in CATEGORIES_PV.items():
            sorted_slugs.append(slug)
            categories_translated[slug] = {'slug': slug, 'name': _(cat["name"]), 'icon_name': cat["icon_name"], 'icon_marker_color': cat["icon_marker_color"]}
        output = {'sorted_slugs': sorted_slugs, 'categories': categories_translated}
        print(output)
        return HttpResponse(json.dumps(output), content_type='application/json')


    @route(r'^getCategories/lang/([a-z][a-z])/$')
    def show_categories(self, request, lang=None):
        # current_lang = (lang or get_language_from_path(request.path_info) or LANGUAGE_CODE)
        activate_translations(lang)
        lang = request.GET.get('l', '')
        current_lang = (lang or LANGUAGE_CODE)

        # print('lang: ', current_lang)
        # print('get_language_from_path(request.path_info): ', get_language_from_path(request.path_info))
        # print('LANGUAGE_CODE: ', LANGUAGE_CODE)
        # activate_translations(current_lang)

        categories_translated = {}
        sorted_slugs = []
        for slug, cat in CATEGORIES.items():
            if slug not in ['socialcurrency', 'socialcurrency.active_network', 'socialcurrency.inactive_network']:
                sorted_slugs.append(slug)
            categories_translated[slug] = {'slug': slug, 'name': _(cat["name"]), 'icon_name': cat["icon_name"], 'icon_marker_color': cat["icon_marker_color"]}

        sorted_slugs.sort()
        sorted_slugs.insert(0, 'socialcurrency')
        sorted_slugs.insert(1, 'socialcurrency.active_network')
        sorted_slugs.insert(2, 'socialcurrency.inactive_network')
        sorted_slugs.remove('faircoop_ln')
        sorted_slugs.insert(3, 'faircoop_ln')

        output = {'sorted_slugs': sorted_slugs, 'categories': categories_translated}
        return HttpResponse(json.dumps(output), content_type='application/json')



    def get_point_short_details(self, lang=None, slug=None):
        from maplisting.models.map_listing_page import MapListingPage
        details = {}
        # current_lang = (lang or get_language_from_path(request.path_info) or LANGUAGE_CODE)
        point = MapListingPage.objects.live().filter(slug=slug).first()
        if point:
            img = point.main_image.get_rendition('width-300').url if point.main_image else None
            details['image_url'] = '{}{}'.format(BASE_URL, img)
            details['description'] = point.intro
            details['title'] = point.title

        return details

    @route(r'^showMapPoint/lang/([a-z][a-z])/slug/([\w\-]+)/$')
    def show_map_point(self, request, lang=None, slug=None):
        from maplisting.models.map_listing_page import MapListingPage
        current_lang = (lang or LANGUAGE_CODE)
        # print('current_lang: ', current_lang)
        activate_translations(current_lang)

        categories_translated = {}
        if slug.startswith('SC_'):
            return show_social_currency_point(request, lang, slug)
        elif slug.startswith('GEN_'):
            return show_gen_ecovillage_point(request, slug)
        elif slug.startswith('FC_'):
            return show_fc_local_node_point(request, slug)
        elif slug.startswith('PV_'):
            return show_sobirania_point(request, slug)

        point = MapListingPage.objects.live().filter(slug=slug).first()

        gallery_images = []
        if point.gallery_images:
            for img in point.gallery_images.all():
                gallery_images.append({'thumb_url': img.image.get_rendition('fill-200x200-c100').url,
                                       'caption': img.caption,
                                       'original_url': img.image.file.url})
        picture_url = point.main_image.get_rendition('width-300').url if point.main_image else None
        cat_dict = {}
        for cat_slug in point.get_other_categories_slugs():
            if cat_slug in CATEGORIES:
                cat_dict[cat_slug] = {'name': CATEGORIES[cat_slug]['name'], 'icon_name': CATEGORIES[cat_slug]['icon_name']}
                if 'icon_prefix' in CATEGORIES[cat_slug]:
                    cat_dict[cat_slug]['icon_prefix'] = CATEGORIES[cat_slug]['icon_prefix']

        intro = point.intro
        title = point.title
        tags = point.tags
        body = point.body
        main_language = point.language
        txt_language = main_language
        preferred_langs = get_preferred_langs(lang)

        translations_qs = MapListingTranslationPage.objects.live().descendant_of(point)
        translations = list(translations_qs.values_list('language', flat=True))
        # print('Translations', translations)

        if main_language != lang and translations:
            for l in preferred_langs:
                if l in translations:
                    t = translations_qs.filter(language=l).specific().first()
                    intro = t.intro
                    title = t.title
                    tags = t.tags
                    body = t.body
                    txt_language = l
                    break
        translations.insert(0, main_language)
        translations.sort()
        dict_translations = []
        for t in translations:
            url = get_url_from_point(point, lang=t)
            dict_translations.append({'iso': t, 'url': url})

        email_parts = []
        if point.email and not point.email.startswith('email@'):
            email_parts = point.email.split("@")
        body = transform_embedded_videos(body)
        html = render(request, 'maplisting/map_right_card.html',{
            'intro': intro, 'title': title, 'body': body,
            'point': point,
            'txt_language': txt_language,
            'translations': dict_translations,
            'gallery_images': gallery_images,
            'picture_url': picture_url,
            'categories': cat_dict,
            'email_parts': email_parts,
            'lon': str(point.location.coords[0]).replace(",","."),
            'lat': str(point.location.coords[1]).replace(",","."),
            'tags': tags.all().values_list('name', flat=True)}
        )
        return HttpResponse(html, content_type='text/html')

    # @route(r'^searchMapPoints/$')

    # def maplistings_layer(self, request, layer, lang=None):
    #     self.maplistings()
    @route(r'^sub/(?P<layer>[\w-]+)/map_points/$')
    @route(r'^map_points/$')
    def maplistings(self, request, layer=None):
        # context = super().get_context(request)
        from maplisting.models.map_listing_page import MapListingPage
        from maplisting.models.map_listing_translation_page import MapListingTranslationPage
        lang = request.GET.get('l', '')
        current_lang = (lang or LANGUAGE_CODE)
        print('current_lang: ', current_lang)
        activate_translations(current_lang)

        json_points = []

        translations_pages = MapListingTranslationPage.objects.live().filter(language=current_lang)
        translations = {}
        for t in translations_pages:
            parent_id = t.get_parent().id
            translations[parent_id] = {'name': t.title, 'description': t.intro}

        load_socialcurrencies = False
        load_faircoin_nodes = False
        load_database_points = True

        if layer == 'faircoin':
            query = MapListingPage.objects.filter(faircoin_use=True).live()
        elif layer in ['monedasocial', 'socialcurrency', 'SEL']:
            query = MapListingPage.objects.filter(socialcurrency_use=True).live()
            load_socialcurrencies = True
        elif layer in ['ecoaldeas', 'ecovillages']:
            load_database_points = False
            if not MapListingIndexPage.ecovillage_points:
                MapListingIndexPage.ecovillage_data, MapListingIndexPage.ecovillage_points = get_ecovillage_points()
            json_points += MapListingIndexPage.ecovillage_points
        elif layer == 'sobiranialimentariapv':
            load_database_points = False
            if not MapListingIndexPage.sobirania_points:
                MapListingIndexPage.sobirania_data, MapListingIndexPage.sobirania_points = get_sobirania_points()
            json_points += MapListingIndexPage.sobirania_points
        else:
            load_socialcurrencies = True
            load_faircoin_nodes = True
            query = MapListingPage.objects.live()

        if load_faircoin_nodes:
            if not MapListingIndexPage.fc_local_nodes_points:
                MapListingIndexPage.fc_local_nodes_data, MapListingIndexPage.fc_local_nodes_points = get_fc_local_nodes_points()
            json_points += MapListingIndexPage.fc_local_nodes_points
        if load_socialcurrencies:
            if not MapListingIndexPage.social_currency_points:
                MapListingIndexPage.social_currency_data, MapListingIndexPage.social_currency_points = get_social_currency_points()
            json_points += MapListingIndexPage.social_currency_points

        if load_database_points:
            database_pages = query.filter(location__isnull=False).order_by('-first_published_at')
            for point in database_pages:
                json_p = get_feature(point.location.coords[0], point.location.coords[1])
                if point.id in translations: #it has a translation with the current language
                    json_p['properties']['name'] = translations[point.id]['name']
                    json_p['properties']['description'] = translations[point.id]['description']
                else:
                    json_p['properties']['name'] = point.title
                    json_p['properties']['description'] = point.intro


                categories = []
                cat_slug = point.map_icon if point.map_icon else 'nocategory'

                if cat_slug in CATEGORIES:
                    json_p['properties']['icon_name'] = CATEGORIES[cat_slug]['icon_name']
                    if 'icon_marker_color' in CATEGORIES[cat_slug]:
                        json_p['properties']['icon_marker_color'] = CATEGORIES[cat_slug]['icon_marker_color']
                else:
                    json_p['properties']['icon_name'] = 'question'
                    json_p['properties']['icon_marker_color'] = 'red'
                    json_p['properties']['icon_prefix'] = 'fa'

                json_p['properties']["slug"] = point.slug
                other_categories = point.get_other_categories_slugs()

                cat_dict = {}
                if other_categories:
                    # categories = set(other_categories)
                    for cat_slug in other_categories:
                        if cat_slug in CATEGORIES:
                            cat_dict[cat_slug] = {'name': CATEGORIES[cat_slug]['name'],
                                                  'icon_name': CATEGORIES[cat_slug]['icon_name']}
                            if 'icon_prefix' in CATEGORIES[cat_slug]:
                                cat_dict[cat_slug]['icon_prefix'] = CATEGORIES[cat_slug]['icon_prefix']

                json_p['properties']["categories"] = cat_dict
                # json_p['properties']["tags"] = list(point.tags.all().values_list('name', flat=True))
                json_points.append(json_p)
        #endfor


        response_json = {"response": json_points, "status": 200} #TODO replace 200 with constant
        #response_json = {"response": [], "status": 200} #TODO replace 200 with constant
        return HttpResponse(json.dumps(response_json), content_type='application/json')

    def get_meta_opengraph(self, placeid, layer):
        details = {}
        title = 'Mapo (Komun.org)'
        if layer in ['ecovillages', 'ecoaldeas']:
            description = '[ES] Ecoaldeas GEN a nivel mundial / [EN] All GEN ecovillages worldwide.'
        else:
            description = '[ES] Monedas sociales y sitios FairCoin en el mundo / [EN] Social currencies and Faircoin accepting places worldwide.'
        image_url = 'https://mapo.komun.org/statics/img/mapo300x200.png'
        url = 'https://mapo.komun.org'
        if placeid:
            if placeid.startswith('SC_'):
                details = get_social_currency_point_short_details(slug=placeid)
            else:
                details = self.get_point_short_details(slug=placeid)
                # print('details', details)
            if details:
                title = details['title']
                description = details['description']
                image_url = details['image_url']
                url = 'https://mapo.komun.org?id=' + placeid

        #Add twitter cards and fb
        meta_opengraph = """
            <meta name="description" itemprop="description" content="{1}" />
            <title>{0}</title>
            <meta property="title" content="{0}" />
            <meta property="og:title" content="{0}" />
            <meta property="description" content="{1}" />
            <meta property="og:description" content="{1}" />
            <meta property="og:image" content="{2}" />
            <meta property="og:url" content="{3}" />
            """.format(title, description, image_url, url)

        return meta_opengraph

    def get_context(self, request, layer=None):
        from django.utils.translation import LANGUAGE_SESSION_KEY
        from maplisting.models.map_listing_page import MapListingPage
        context = super().get_context(request)

        lang_parameter = request.GET.get('l', '')
        current_lang = (lang_parameter or request.LANGUAGE_CODE or LANGUAGE_CODE)
        context['language'] = current_lang
        print('current_lang', current_lang)
        activate_translations(current_lang)
        request.session[LANGUAGE_SESSION_KEY] = current_lang
        print('Loading sublayer', layer)
        placeid = request.GET.get('id', '')
        lang = request.GET.get('l', '')
        load_socialcurrencies = False
        show_last_week = True
        template_base = 'index'
        context['meta_opengraph'] = self.get_meta_opengraph(placeid, layer)
        if layer == 'faircoin':
            query = MapListingPage.objects.filter(faircoin_use=True).live()
        elif layer in ['monedasocial', 'socialcurrency', 'SEL']:
            query = MapListingPage.objects.filter(socialcurrency_use=True).live()
            load_socialcurrencies = True

        elif layer in ['ecoaldeas', 'ecovillages']:
            show_last_week = False
            if not MapListingIndexPage.ecovillage_points:
                MapListingIndexPage.ecovillage_data, MapListingIndexPage.ecovillage_points = get_ecovillage_points()
            print('Imported ', len(MapListingIndexPage.ecovillage_points), 'GEN points')
            total_points = len(MapListingIndexPage.ecovillage_points)

        elif layer == 'sobiranialimentariapv':
            MapListingIndexPage.sobirania_data, MapListingIndexPage.sobirania_points = get_sobirania_points()
            total_points = len(MapListingIndexPage.sobirania_points)
            print('Imported ', total_points, 'PV points')
            show_last_week = False
            template_base = 'index.sobirania'

        else:
            load_socialcurrencies = True
            query = MapListingPage.objects.live()

        if show_last_week:
            total_points = query.filter(location__isnull=False).count()
        if load_socialcurrencies:
            if not MapListingIndexPage.social_currency_points:
                MapListingIndexPage.social_currency_data, MapListingIndexPage.social_currency_points = get_social_currency_points()
            total_points += len(MapListingIndexPage.social_currency_points)

        path = os.path.join(PROJECT_DIR, 'templates/frontend/statics/{}.js'.format(template_base))
        modifdatetime_js = int(os.path.getmtime(path))
        context["index_js_version"] = modifdatetime_js
        context["template_base"] = template_base
        context["template_base_file"] = 'frontend/{}.html'.format(template_base)
        # if show_last_week:
        #     today = date.today()
        #     last_week = today - timedelta(days=7)
        #     total_last_week = query.live().filter(first_published_at__gte=last_week).count()
        #     context['map_header'] = '{} points (+{} last week)'.format(total_points, total_last_week)
        # else:
        if layer == 'sobiranialimentariapv':
            context['map_header'] = '{} punts'.format(total_points)
        else:
            context['map_header'] = _('{} points').format(total_points)


        return context

    @route(r'^sub/([a-z]+)/$')
    def load_layer(self, request, layer=None):
        return TemplateResponse(
          request,
          self.get_template(request),
          self.get_context(request, layer)
        )
    class Meta:
        verbose_name = "Map Portal"
