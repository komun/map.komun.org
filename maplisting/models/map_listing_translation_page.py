
from django import forms
from django.contrib.gis.db import models
from modelcluster.fields import ParentalKey
from modelcluster.contrib.taggit import ClusterTaggableManager
from taggit.models import TaggedItemBase
from django.utils.translation import ugettext_lazy as _
from wagtail.core.models import Page
from wagtail.core.fields import RichTextField
from wagtail.admin.edit_handlers import FieldPanel
from wagtail.search import index
from mapo.settings.base import LANGUAGES

from maplisting.utils import get_url_from_point

from mapo.settings.base import BASE_URL


class MapListingTranslationPageTag(TaggedItemBase):
    content_object = ParentalKey(
        'MapListingTranslationPage',
        related_name='tagged_items',
        on_delete=models.CASCADE
    )


class MapListingTranslationPage(Page):
    # page = ParentalKey('MapListingPage', on_delete=models.PROTECT, related_name='translations')
    language = models.CharField(max_length=5, choices=LANGUAGES, default='en', verbose_name=_("Page language"))

    intro = models.CharField(verbose_name=_("Short description"), max_length=250, null=True, blank=True)
    body = RichTextField(verbose_name=_("Long description"), blank=True, features=['h3','h4', 'h5', 'h6', 'ol', 'ul', 'hr', 'bold', 'italic', 'link', 'embed'])
    tags = ClusterTaggableManager(through=MapListingTranslationPageTag, blank=True, verbose_name=_('Translated tags'))

    search_fields = Page.search_fields + [
        index.SearchField('intro'),
        index.SearchField('body'),
    ]

    parent_page_types = ['MapListingPage']
    subpage_types = []

    content_panels = Page.content_panels + [
        FieldPanel('language', widget=forms.Select),
        FieldPanel('intro'),
        FieldPanel('body'),
        FieldPanel('tags'),
    ]

    def get_admin_display_title(self):
        return '{} (u:{}, l:{})'.format(self.title, self.owner, self.language)

    def get_url_parts(self, *args, **kwargs):
        from maplisting.models.map_listing_page import MapListingPage
        id, _, _ = super().get_url_parts(*args, **kwargs)
        main_page = MapListingPage.objects.parent_of(self).first()
        url_relative = get_url_from_point(main_page, lang=self.language)
        return id, BASE_URL, url_relative

    class Meta:
        verbose_name = _("Page Translation")
