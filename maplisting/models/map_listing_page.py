import collections
from django.core.validators import RegexValidator
from django.utils.safestring import mark_safe
from django.utils.functional import lazy
from django.utils import six 
mark_safe_lazy = lazy(mark_safe, six.text_type)

from django import forms
from django.contrib.gis.db import models
from modelcluster.fields import ParentalKey
from modelcluster.contrib.taggit import ClusterTaggableManager
from taggit.models import TaggedItemBase
from django.utils.translation import get_language, ugettext_lazy as _ , string_concat
from wagtailgeowidget.edit_handlers import GeoPanel
from wagtail.core.models import Page, Orderable
from wagtail.core.fields import RichTextField
from wagtail.admin.edit_handlers import FieldPanel, InlinePanel, MultiFieldPanel
from wagtail.images.edit_handlers import ImageChooserPanel
from wagtail.search import index
from wagtail.contrib.routable_page.models import RoutablePageMixin

from django.contrib.auth.models import Group
from registration.signals import user_activated, user_registered

from mapo.settings.base import LANGUAGES
from django.dispatch import receiver

from maplisting.categories import CATEGORIES
from maplisting.utils import get_preferred_langs, get_url_from_point
from maplisting.models import ChoiceArrayField

from maplisting.models.map_listing_translation_page import MapListingTranslationPage

from mapo.settings.base import BASE_URL

# class MapListingTagIndexPage(Page):
#
#     def get_context(self, request):
#         # Filter by tag
#         tag = request.GET.get('tag')
#         maplistingpages = MapListingPage.objects.filter(tags__name=tag)
#
#         # Update template context
#         context = super().get_context(request)
#         context['maplistingpages'] = maplistingpages
#         return context

class MapListingPageTag(TaggedItemBase):
    content_object = ParentalKey(
        'MapListingPage',
        related_name='tagged_items',
        on_delete=models.CASCADE
    )


class MapListingPageGalleryImage(Orderable):
    page = ParentalKey('MapListingPage', on_delete=models.CASCADE, related_name='gallery_images')
    image = models.ForeignKey('wagtailimages.Image', verbose_name=_('Image'), on_delete=models.CASCADE, related_name='+')
    caption = models.CharField(verbose_name=_('Caption'), blank=True, max_length=250)

    panels = [
        ImageChooserPanel('image'),
        FieldPanel('caption'),
    ]


@receiver(user_activated)
@receiver(user_registered)
def user_registered(sender, user, request, **kwargs):
    editors_group = Group.objects.filter(name='Editors').first()
    user.groups.add(editors_group)

CAT_CHOICES = ()
ICON_CHOICES = ()
od = collections.OrderedDict(sorted(CATEGORIES.items()))
for cat_slug, cat in od.items():
    if cat_slug not in ['socialcurrency', 'socialcurrency.inactive_network', 'socialcurrency.active_network']:
        if '.' in cat_slug:
            cat_name = string_concat("─"*3,_(cat['name']))
        else:
            cat_name = string_concat("»» ",_(cat['name']))
        
        new_choice = (cat_slug, cat_name)
        CAT_CHOICES += (new_choice,)
        if cat_slug not in ['coop', 'faircoop']:
            ICON_CHOICES += (new_choice,)

from maplisting.models.map_listing_index_page import MapListingIndexPage
class MapListingPage(RoutablePageMixin, Page):
    address = models.CharField(verbose_name=_("Find location in map"), max_length=250, blank=True, null=True, help_text=_("Not publicly displayed, only to locate the point in the map."))
    public_address = models.CharField(verbose_name=_("Public Address"),max_length=250, blank=True, null=True, help_text=_("If you don't want to show your public address, leave empty."))
    location = models.PointField(verbose_name=_("Coordinates"),srid=4326, null=True, blank=True)
    intro = models.CharField(verbose_name=_("Short description"), max_length=250, null=True, blank=False, help_text=_('250 characters max.'))
    body = RichTextField(verbose_name=_("Long description"), blank=False, features=['h4', 'h5', 'h6', 'ol', 'ul', 'hr', 'bold', 'italic', 'link', 'embed'])
    tags = ClusterTaggableManager(through=MapListingPageTag, blank=True, verbose_name=_('Tags'), help_text=_('A comma-separated list of tags.'))
    language = models.CharField(max_length=5, choices=LANGUAGES, default='en', verbose_name=_("Language"))
    map_icon = models.CharField(max_length=30, choices=ICON_CHOICES, verbose_name=_("Map Icon"), blank=False, null=True,
                help_text=_('The main icon to be shown in the map.'))

    other_categories = ChoiceArrayField(models.CharField(max_length=30,  choices=CAT_CHOICES), blank=True, null=True,
                                        verbose_name=_("Other categories"),
                                        help_text=_('Select several categories by holding the CTRL key.'))#, default=['defect'])
    main_image = models.ForeignKey('wagtailimages.Image', on_delete=models.SET_NULL, related_name='+',
                                   verbose_name=_("Logo or main image"), blank=False, null=True,
                                   help_text=_('It will be automatically resized to 300px width.'))
    #Additional info
    phone = models.CharField(verbose_name=_("Phone"), max_length=30, blank=True, null=True, help_text=_('Please add the international prefix.'))
    website = models.URLField(verbose_name=_("Website"), blank=True, null=True)
    twitter = models.URLField(verbose_name=_("Twitter"), blank=True, null=True, help_text='https://twitter.com/username')
    facebook = models.URLField(verbose_name=_("Facebook"), blank=True, null=True, help_text='https://facebook.com/username')
    telegram = models.URLField(verbose_name=_("Telegram"), blank=True, null=True, help_text='https://t.me/username')
    mastodon = models.URLField(verbose_name=_("Fediverse"), blank=True, null=True,
                               help_text=mark_safe_lazy(_('Add your Fediverse account (e.g. Mastodon), is much better than FB or Twitter. ' \
                                                    'Check out our <a target="_blank" href="https://hostux.social/web/accounts/68683">account</a> and follow us!')))
    email = models.EmailField(verbose_name=_("Email"), blank=True, null=True)
    #TODO: merge opening_hours at the bottom of the body and delete this field in a migration
    #opening_hours = models.CharField(max_length=250, blank=True, null=True)
    faircoin_address = models.CharField(verbose_name=_("Faircoin Address"), validators=[RegexValidator(regex='^[fF][a-zA-Z0-9]{33}$', message=_('Invalid FairCoin address'), code='nomatch')],
                                        max_length=34, blank=True, null=True,
                                        help_text=mark_safe_lazy(_('You can learn more about FairCoin, download a free wallet <a target="_blank" href="https://komun.org/en/blog/faircoin-conceptos-fundamentales">here</a> and then add your address!')))
    faircoin_use = models.BooleanField(verbose_name=_("Do you accept FairCoin?"), default=True)
    show_qr = models.BooleanField(verbose_name=_("Show my FairCoin address QR for donations"), default=True)

    socialcurrency_use = models.BooleanField(verbose_name=_("Do you accept LETS/social currency?"), default=False, help_text=mark_safe_lazy(_('Learn more about <a target="_blank" href="https://en.wikipedia.org/wiki/Local_exchange_trading_system">LETS</a> or <a target="_blank" href="https://en.wikipedia.org/wiki/Local_currency">local social currencies</a>.')))
    socialcurrency_account = models.CharField(verbose_name=_("Name of your LETS/Local social currency network"), blank=True, null=True, max_length=30, help_text=_('If affirmative, tell us which one do you accept, (e.g. EXSV, ECOS, Wir franc, Bristol pound, etc.)'))


    def get_other_categories_slugs(self):
        main_cat = self.map_icon
        if not self.other_categories:
            other_categories = set()
        else:
            other_categories = set(self.other_categories)
        if main_cat:
            other_categories.add(main_cat)
        if self.socialcurrency_use:
            other_categories.add('socialcurrency')
        return other_categories

    search_fields = Page.search_fields + [
        index.SearchField('intro'),
        index.SearchField('body'),
        index.SearchField('email'),
        index.SearchField('title'),
        index.FilterField('first_published_at'),
        index.FilterField('language'),
    ]

    parent_page_types = ['MapListingIndexPage']
    subpage_types = ['MapListingTranslationPage']

    content_panels = Page.content_panels + [
        MultiFieldPanel([
            FieldPanel('language', widget=forms.Select),
            FieldPanel('intro'),
            FieldPanel('body'),
            FieldPanel('tags'),
        ], heading=_("Information")),

        MultiFieldPanel([
            FieldPanel('map_icon', widget=forms.Select),
            FieldPanel('other_categories'),
        ], heading=_("Categories")),
        MultiFieldPanel([
            FieldPanel('address'),
            GeoPanel('location', address_field='address'),
            FieldPanel('public_address'),
        ], _('Location')),
        MultiFieldPanel([
            FieldPanel('faircoin_use'),
            FieldPanel('show_qr'),
            FieldPanel('faircoin_address'),
            FieldPanel('socialcurrency_use'),
            FieldPanel('socialcurrency_account'),
        ], heading=_("Alternative currencies")),
        ImageChooserPanel('main_image'),
        InlinePanel('gallery_images', label=_("Gallery images")),
        MultiFieldPanel([
            FieldPanel('phone'),
            FieldPanel('email'),
            FieldPanel('telegram'),
            FieldPanel('website'),
            FieldPanel('mastodon'),
            FieldPanel('twitter'),
            FieldPanel('facebook'),
            # FieldPanel('opening_hours'),
        ], _('Contact info')),

    ]
    settings_panels = Page.settings_panels + [
        FieldPanel('owner'),
        # FieldPanel('first_published_at'),
    ]

    def get_admin_display_title(self):
        return '{} (u:{}, l:{})'.format(self.title, self.owner, self.language)

    def get_url_parts(self, *args, **kwargs):
        id, _, _ = super().get_url_parts(*args, **kwargs)
        url_relative = get_url_from_point(self, lang=self.language)
        return id, BASE_URL, url_relative

    class Meta:
        verbose_name = _("Map Point")
        verbose_name_plural = _("Map Points")

    def get_context(self, request):
        context = super().get_context(request)
        # maplistingpages = MapListingPage.objects.filter(tags__name=tag)
        browser_lang = get_language()
        # import pdb
        # pdb.set_trace()

        page = self
        gallery_images = page.gallery_images
        intro = page.intro
        title = page.title
        tags = page.tags
        body = page.body
        main_language = page.language
        txt_language = main_language
        preferred_langs = get_preferred_langs(browser_lang)

        translations_qs = MapListingTranslationPage.objects.live().descendant_of(self)
        translations = list(translations_qs.values_list('language', flat=True))
        # print('Translations', translations)

        if main_language != browser_lang and translations:
            for l in preferred_langs:
                if l in translations:
                    t = translations_qs.filter(language=l).specific().first()
                    intro = t.intro
                    title = t.title
                    tags = t.tags
                    body = t.body
                    txt_language = l
                    break

        translations.insert(0, main_language)
        translations.sort()
        dict_translations = []
        for t in translations:
            url = '/{}/dir/{}'.format(t, page.slug)
            dict_translations.append({'iso': t, 'url': url})
        context['translations'] = dict_translations
        context['txt_language'] = txt_language
        context['page'] = page
        context['current_lang'] = browser_lang
        context['intro'] = intro
        context['gallery_images'] = gallery_images
        context['title'] = title
        context['tags'] = tags
        context['body'] = body
        return context
