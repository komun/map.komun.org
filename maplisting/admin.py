# Register your models here.
from django.contrib import admin
from maplisting.models.map_listing_page import MapListingPage
from maplisting.models.map_listing_translation_page import MapListingTranslationPage

class MapListingPageAdmin(admin.ModelAdmin):
    list_display = ('id', 'first_published_at', 'title', 'intro', 'language')
    list_filter = ('first_published_at', 'language')
    search_fields = ('id', 'title', 'intro', 'body', )
admin.site.register(MapListingPage, MapListingPageAdmin)

class MapListingTranslationPageAdmin(admin.ModelAdmin):
    list_display = ('id', 'first_published_at', 'title', 'intro', 'language')
    list_filter = ('first_published_at', 'language')
    search_fields = ('id', 'title', 'intro', 'body', )
admin.site.register(MapListingTranslationPage, MapListingTranslationPageAdmin)
