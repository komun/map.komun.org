#!/usr/bin/env python
# -*- coding: utf-8 -*-
from __future__ import print_function

from django.template.defaultfilters import slugify

from django.core.management.base import BaseCommand
from django.core.management import call_command
import requests
from lxml import html
import re
import pickle
import os.path
import json
from robobrowser import RoboBrowser

from mapo.settings.base import PROJECT_DIR
import pprint

GOOGLE_MAPS_V3_APIKEY = 'AIzaSyBAWD-wtWudkCqJx3Hq3PVC9uL106HB2zg'
import googlemaps

gmaps = googlemaps.Client(key=GOOGLE_MAPS_V3_APIKEY)

def check_url_status(url):
    if url != 'http://www.community-exchange.org' and url != 'http://www.ces.org.za':
        try:
            r = requests.get(url)
            return r.status_code
        except:
            return 0
    else:
        return 200

def detect_body_language(body):
    try:
        clean_body = cleanhtml(body)
        lang = detect(clean_body)
        if lang not in valid_langs:
            print('Language:', lang, ' not found for: ', clean_body)
            lang = 'en'

    except:
        lang = 'en'
    return lang


class Command(BaseCommand):
    help = 'Import from CES and IntegralCES and save it into a JSON'

    def handle(self, *args, **options):
        # if options['integral_ces']:
        #     call_command('flush_models')
        # scrapping_ces(scope='AUSTRALIA')
        scrapping_ces(scope='world')
        # scrapping_ices()
        # scrapping_communityforge()

def login_au_ces():
    browser = RoboBrowser(parser="lxml")
    browser.open('https://www.communityexchange.net.au/login.asp')
    # Get the signup form
    signup_form = browser.get_forms()[0]
    signup_form['memno'].value = 'HUNT0634'
    signup_form['Passwd'].value = '7yeonyie7YEONYIE'
    # Submit the form
    browser.submit_form(signup_form)
    return browser

def login_ces():
    browser = RoboBrowser(parser="lxml")
    browser.open('https://www.community-exchange.org/login.asp')
    # Get the signup form
    signup_form = browser.get_forms()[0]
    signup_form['memno'].value = 'EXSV0001'
    signup_form['Passwd'].value = 'cmndsntg4'
    # Submit the form
    browser.submit_form(signup_form)
    return browser

def login_ices():
    browser = RoboBrowser(parser="lxml")
    browser.open('https://integralces.net/es/user')
    # Get the signup form
    signup_form = browser.get_forms()[0]
    signup_form['name'].value = 'Komun'
    signup_form['pass'].value = 'Komun2018#&'
    # Submit the form
    browser.submit_form(signup_form)
    return browser


def get_location_from_gmap_results(gmaps_results, country):
    location = None
    if country == 'UK':
        country = 'GB'

    for r in gmaps_results:
        for c in r['address_components']:
            if 'country' in c['types']:
                country_code = c['short_name']
                if not country or country_code == country:
                    location = r['geometry']['location']
                    return location
    return location

def scrapping_ices():
    print('Scrapping integralCES')
    all_networks_url = 'https://integralces.net/es/ces/bank/exchange/otherexchanges/list'
    browser = login_ices()
    browser.open(all_networks_url)
    parser = html.HTMLParser(encoding=str('utf-8'))
    tree = html.fromstring(browser.response.content, parser=parser)
    ids = tree.xpath('//tbody//tr//td[1]/text()')
    names = tree.xpath('//tbody//tr//td[2]/a/text()')
    regions = tree.xpath('//tbody//tr//td[3]/text()')
    towns = tree.xpath('//tbody//tr//td[4]/text()')
    accounts = tree.xpath('//tbody//tr//td[5]/text()')
    currencies = tree.xpath('//tbody//tr//td[6]/text()')

    all_networks = {}
    print('names', len(names))
    print('regions', len(regions))
    print('towns', len(towns))
    print('accounts', len(accounts))
    print('currencies', len(currencies))

    for i, id in enumerate(ids):
        name = names[i]
        region = regions[i]
        town = towns[i]
        total_accounts = accounts[i]
        currency = currencies[i]
        address = '{}, {}'.format(town, region)
        print('Searching address', address)
        gmaps_results = gmaps.geocode(address)
        coords = get_location_from_gmap_results(gmaps_results, "")
        network = {"active": True, "platform": "ICES", "currency": currency, "total_accounts": total_accounts,
                   "name": name, "network": id}

        if coords:
            network['lat'] = float(coords['lat'])
            network['lon'] = float(coords['lng'])
            print('Found location at', coords)
        else:
            network['lat'] = ''
            network['lon'] = ''
            print('Not found location', id)
        all_networks[id] = network

    path = os.path.join(PROJECT_DIR, 'ices_social_currencies.json')
    print('Saving json to', path)
    with open(path, 'w+') as outfile:
        json.dump(all_networks, outfile, indent=4)
    print('Saved.')


def scrapping_ces(scope='world'):
    print('Scrapping CES')
    browser = None
#### STEP 1 https://www.community-exchange.org/docs/joinexchange.asp -> get all countries
    if scope == 'world':
        if os.path.exists('/tmp/ces.countries.pickle'):
            print('Loading /tmp/ces.countries.pickle')
            with open('/tmp/ces.countries.pickle', 'rb') as handle:
                countries = pickle.load(handle)
        else:
            countries = []
            print('Downloading https://www.community-exchange.org/docs/joinexchange.asp')
            page = requests.get('https://www.community-exchange.org/docs/joinexchange.asp')
            print('Parsing...')
            if page.text:
                for i, line in enumerate(page.text.split("\n")):
                    # print('>', line)
                    countries_search = re.search('joinexchange.asp\?country=(..)&sort=place&close"', line, re.IGNORECASE)
                    if countries_search:
                        code = countries_search.group(1)
                        if code not in ['AU', 'WO']:  # Discarded World ones, also Australia in CES is moved to https://www.communityexchange.net.au
                            countries.append(code)

        print(countries)
        if countries:
            with open('/tmp/ces.countries.pickle', 'wb') as handle:
                pickle.dump(countries, handle, protocol=pickle.HIGHEST_PROTOCOL)


#### STEP 2 https://www.community-exchange.org/joinexchange.asp?country=AR  -> get all networks ids from each country
    if scope == 'AUSTRALIA':
        domain = 'https://www.communityexchange.net.au'
        countries = ['AU']
    else:
        domain = 'https://www.community-exchange.org'
    if os.path.exists('/tmp/ces.networks.pickle'):
        print('Loading /tmp/ces.networks.pickle')
        with open('/tmp/ces.networks.pickle', 'rb') as handle:
            networks = pickle.load(handle)
    else:
        networks = {}
        for code in countries:
            # code = 'AR'

            url = '{}/joinexchange.asp?country={}'.format(domain, code)
            print('Downloading ', url)
            page = requests.get(url)
            print('Parsing...')

            parser = html.HTMLParser(encoding=str('utf-8'))
            tree = html.fromstring(page.content, parser=parser)
            ids = tree.xpath('//tr[@class="textsmall"]//td[2]/text()')
            network_names = tree.xpath('//tr[@class="textsmall"]//td[3]/a/text()')

            place_names = tree.xpath('//tr[@class="textsmall"]//td[4]/a/text()')

            province_names = tree.xpath('//tr[@class="textsmall"]//td[5]/text()')
            if len(place_names) != len(network_names) or len(place_names) != len(province_names):
                print(network_names)
                print(place_names)
                print(province_names)
                b
            comments = tree.xpath('//tr[@class="textsmall"]//td[7]')
            for i, network_id in enumerate(ids):
                network_name = network_names[i]
                comment = comments[i]
                province = province_names[i]
                place = place_names[i]
                url = ''
                for k in comment:
                    if str(k.tag) == 'a':
                        url = k.text
                comment = "" if not comment.text or len(comment.text) < 3 else comment.text
                if scope == 'world':
                    platform = 'CES'
                else:
                    platform = 'AU_CES'
                networks[network_id] = {'comment': comment, 'url': url,
                                        'place': place, 'province': province, 'platform': platform,
                                        'name': network_name, 'country': code, 'network': network_id}

        if networks:
            with open('/tmp/ces.networks.pickle', 'wb') as handle:
                pickle.dump(networks, handle, protocol=pickle.HIGHEST_PROTOCOL)


    print('Networks imported', len(networks))
    if scope == 'AUSTRALIA':
        if not browser: browser = login_au_ces()
    else:
        if not browser: browser = login_ces()  # Logging in CES
    # get stats from https://www.community-exchange.org/statstrading.asp?xid=EXSV (discard those not active since 3 months)
    if os.path.exists('/tmp/ces.activity_networks.pickle'):
        print('Loading /tmp/ces.activity_networks.pickle')
        with open('/tmp/ces.activity_networks.pickle', 'rb') as handle:
            activity_networks = pickle.load(handle)
    else:
        activity_networks = {}

        active_months = ['{}\xa0{}'.format('setembre', '2018'),
                         '{}\xa0{}'.format('octubre', '2018'),
                         '{}\xa0{}'.format('novembre', '2018'),
                         '{}\xa0{}'.format('desembre', '2018'),
                         ]

        all_networks_url = '{}/exchanges.asp'.format(domain)
        browser.open(all_networks_url)
        parser = html.HTMLParser(encoding=str('utf-8'))
        tree = html.fromstring(browser.response.content, parser=parser)

        #Now is Logged in and can scrape other pages
        column_code = tree.xpath('//tr[@valign="top"]/td[2]/text()')
        column_nid = tree.xpath('//tr[@valign="top"]/td[3]/text()')
        column_name = tree.xpath('//tr[@valign="top"]/td[4]/a/text()')
        column_currency = tree.xpath('//tr[@valign="top"]/td[5]/text()')
        column_type = tree.xpath('//tr[@valign="top"]/td[6]/text()')
        column_created = tree.xpath('//tr[@valign="top"]/td[10]/text()')

        for i, code in enumerate(column_code):
            new_info = {'NID': column_nid[i], 'name': column_name[i],
                                              'type': column_type[i], 'created_date': column_created[i],
                                              'currency': column_currency[i]}

            if code in networks:
                networks[code].update(new_info)
                url = networks[code]['url']
                if url and not url.startswith('http'):
                    url = 'http://' + url
                if url == 'http://www.community-exchange.org' or url == 'http://www.ces.org.za':
                    url = ''
                networks[code]['url'] = url
                #JOIN LINK https://www.community-exchange.org/home/ces-exchanges/?xid=TNPO
                # if url:
                #     status = check_url_status(url)

                #

        stats_active = 0
        stats_inactive = 0
        stats_unused = 0
        for i, code in enumerate(networks.keys()):
            # if i > 15: break  # just for testing
            country = networks[code]['country']
            if scope == 'world' and country in ['AU', 'WO']:
                continue
            stats_url = '{}/statstrading.asp?xid={}'.format(domain, code)
            print(i, 'Checking last activity', stats_url)
            browser.open(stats_url)
            parser = html.HTMLParser(encoding=str('utf-8'))
            tree = html.fromstring(browser.response.content, parser=parser)
            last_months = tree.xpath('//tr[@valign="top"]/td[2]/text()')
            num_tx_average = tree.xpath('//tr[@class="textbold totals"]/td[3]/text()')
            amount_average = tree.xpath('//tr[@class="textbold totals"]/td[4]/text()')
            num_tx = tree.xpath('//tr[@valign="top"]/td[3]/text()')
            amount_tx = tree.xpath('//tr[@valign="top"]/td[4]/text()')
            # print(last_months)

            transactions = []
            active = False
            min_activity = 3
            if len(last_months) > min_activity:
                min_row = len(last_months) - min_activity - 1
                max_row = len(last_months)-1
                for m, month in enumerate(last_months):
                    if m in range(min_row, max_row):
                        transactions.append({'month': month.replace('\u00a0', ' '), 'num_tx': num_tx[m], 'amount_tx': amount_tx[m]})
                        if month in active_months:
                            active = True

                if active:
                    print('Active')
                    activity_networks[code] = networks[code]
                    activity_networks[code]['num_tx_average'] = num_tx_average[0]
                    activity_networks[code]['amount_average'] = amount_average[0]
                    activity_networks[code]['transactions'] = transactions
                    activity_networks[code]['logo'] = ''
                    activity_networks[code]['active'] = True
                    stats_active += 1
                else:
                    print('Inactive')
                    activity_networks[code] = networks[code]
                    activity_networks[code]['num_tx_average'] = num_tx_average[0]
                    activity_networks[code]['amount_average'] = amount_average[0]
                    activity_networks[code]['last_month'] = last_months[max_row-1]
                    activity_networks[code]['logo'] = ''
                    activity_networks[code]['active'] = False
                    stats_inactive += 1
            else:
                stats_unused += 1
                print('Never functioned')

        print('unused_networks', stats_unused)
        print('inactive_networks', stats_inactive)
        print('active_networks', stats_active)
        if activity_networks:
            with open('/tmp/ces.activity_networks.pickle', 'wb') as handle:
                pickle.dump(activity_networks, handle, protocol=pickle.HIGHEST_PROTOCOL)

    import pprint
    pprint.pprint(activity_networks)
    print('activity_networks', len(activity_networks))

    # get location and do a georeference (get a relation area and its capital from Nominatum/OSM) https://www.openstreetmap.org/relation/340544
    from nominatim import Nominatim
    osm = Nominatim(base_url='http://nominatim.openstreetmap.org')
    found_location = 0
    i = 0
    todelete = []
    for id, n in activity_networks.items():
        i += 1
        # if i > 10: break  # just for testing
        place = n['place']
        province = n['province']
        country = n['country']
        if scope == 'world' and country in ['AU', 'WO']:
            todelete.append(id)
            continue
        address = '{}, {}'.format(place, province)
        print(i, '->', address, ',', country)
        best_result = None
        results = []
        if place and place != 'Map':
            results = osm.query(address, countrycodes=[country], limit=100)
        # print(results)

        osm_id = ''
        for r in results:
            if 'osm_type'in r and r['osm_type'] == 'relation':
                best_result = r
                osm_id = r['osm_id']
                break
        if not best_result and len(results):
            best_result = results[0]

        n['osm_id'] = osm_id
        if best_result:
            print(id, '->', best_result['display_name'])
            # print(best_result['osm_type'])
            print('{},{}'.format(best_result['lat'], best_result['lon']))
            n['lat'] = best_result['lat']
            n['lon'] = best_result['lon']
            found_location += 1

        else:
            print(id, '-> No geolocalization')
            info_url = '{}/exchshow.asp?xid={}'.format(domain, id)
            print(i, 'Checking more info', info_url)
            browser.open(info_url)
            parser = html.HTMLParser(encoding=str('utf-8'))
            tree = html.fromstring(browser.response.content, parser=parser)
            coords = None
            location_html = tree.xpath('//tr[@class="text"][2]/td[2]')[0]
            location = location_html.text.strip() if location_html.text else ''
            # import pdb
            # pdb.set_trace()
            for k in location_html:
                if k.tail: location += ', {}'.format(k.tail.strip())
            if location:
                print('Searching location address', location)
                gmaps_results = gmaps.geocode(location)
                coords = get_location_from_gmap_results(gmaps_results, country)
            if not coords:
                address_html = tree.xpath('//tr[@class="text"][5]/td[2]')[0]
                address = address_html.text.strip() if address_html.text else ''
                for k in address_html:
                    address += ', {}'.format(k.tail.strip())
                if address:
                    print('Searching administrator address', address)
                    gmaps_results = gmaps.geocode(address)
                    coords = get_location_from_gmap_results(gmaps_results, country)

            n['lat'] = ''
            n['lon'] = ''
            if coords:
                n['lat'] = float(coords['lat'])
                n['lon'] = float(coords['lng'])
                print('Found location at', coords)


    print('Found locations for', found_location)
    for n in todelete:
        activity_networks.pop(n)
    if scope == 'AUSTRALIA':
        json_file = 'AU_CES_social_currencies.json'
    else:
        json_file = 'CES_social_currencies.json'
    path = os.path.join(PROJECT_DIR, json_file)
    print('Saving json to', path)
    with open(path, 'w+') as outfile:
        json.dump(activity_networks, outfile, indent=4)
    print('Saved.')



# copy last 12 months exchanges (units and nTransactions)
# get active users
# Estadistiques de 1 membre: https://www.community-exchange.org/memactivity.asp?uid=ACESVIRT
# Estadístiques de transaccions  https://www.community-exchange.org/statstrading.asp?xid=ACES
# Stats members: https://www.community-exchange.org/statsmembers.asp?xid=BITS
# get info from each network https://www.community-exchange.org/exchshow.asp?xid=ACES
# Login stats https://www.community-exchange.org/memon.asp?sel=all&xid=EXMA
# Total Active user stats: https://www.community-exchange.org/statsmem.asp?gid=EXMA
# Balance all users: https://www.community-exchange.org/statsbalances.asp (solo EXSV)
#make node graphs of exchanges from each user https://www.community-exchange.org/mytrades.asp?uid=EXMA0088
#get all the connections from a network to other newtorks and make a graph with that too.
# https://www.community-exchange.org/exchparams.asp?country=ALL
# para australia, hacer matching de las ids de https://communityexchange.net.au/joinexchange.asp?country=AU con
# https://www.communityexchange.net.au/exchparams.asp?country=ALL para pillar nombre moneda,


import csv
def scrapping_communityforge():
    print('Scrapping Community Forge')
    networks_csv = 'http://communityforge.net/geo.csv'
    page = requests.get(networks_csv)
    print('Parsing...')
    import re
    new_csv = []
    if page.text:
        for i, line in enumerate(page.text.split("\n")):

            line_org = line
            if i > 0:

                line = re.sub(r'([^,|\n])"([^,|\n])', "\\1`\\2", line)
                line = line.replace('ÃÂ¨','è')
                line = line.replace('Ã\x83Â©','é')
                line = line.replace('Ã\x83Â§','ç').replace('Ã\x83Â','É').replace("â","'")
                line = line.replace('Ã¢ÂÂ', "'")
                line = line.replace('ÃÂ´', "ô")
                line = line.replace('ÃÂª', "ê")
                line = line.replace('ÃÂ', "à")
                line = line.replace('eÌ', "é")
                line = line.replace('ÃÂ', "")
                line = line.replace('ÃÂ', "œ")
                line = line.replace('ÃÂ©', "é")
                line = line.replace('Ã¢ÂÂ¦', "…")
                line = line.replace('à', "Ô")
                line = line.replace('ÃÂ©', "é")
                line = line.replace('à¢', "â")


                new_csv.append(line)
                print(line)


    new_csv_txt = "\n".join(new_csv)
    # print(new_csv_txt)

    cr = csv.reader(new_csv_txt)
    all_networks = []
    row_items = []
    for row in cr:
        if row == ['','']:
            continue
        if not len(row):
            # print(row_items)
            all_networks.append(row_items)
            # print(len(row_items)) #All rows has 10 items
            row_items = []
        else:
            row_items.extend(row)
    all_networks.append(row_items)
    all_networks_json = {}
    print('All networks downloaded', len(all_networks))
    for n in all_networks:
        url = n[0]
        net = {"join_url": url,
               "lat": n[1],
               "lon": n[2],
               "WKT": n[3],
               "name": n[4],
               "comment": n[5],
               "logo": n[6],
               "active_members": n[7],
               "year_transactions": n[8],
               "ads": n[9],
               "platform": "CForge"}
        if not int(net["year_transactions"]):
           net["active"] = False
        else:
           net["active"] = True

        if net["comment"] == "Mettez ici votre slogan":
            net["comment"] = ""
        if not int(net["year_transactions"]) and int(net["active_members"]) < 5:
            continue
        slug_url = slugify(url.replace('.','_'))
        all_networks_json[slug_url] = net
    path = os.path.join(PROJECT_DIR, 'community_forge_social_currencies.json')
    print('Saving json to', path)
    with open(path, 'w+') as outfile:
        json.dump(all_networks_json, outfile, indent=4)
    print('Saved.')
    print('Total networks >4tx', len(all_networks_json))
