from django.utils.translation import ugettext as _

CATEGORIES_PV = {
                'consum': {'name': 'Consum Responsable', 'icon_name': 'consum', 'icon_marker_color': '#445500'},
                'mercats': {'name': 'Mercats camperols', 'icon_name': 'mercats', 'icon_marker_color': '#442178'},
                'produccio': {'name': 'Producció', 'icon_name': 'produccio', 'icon_marker_color': '#aa0000'},
                'recursos': {'name': 'Recursos i Organitzacions', 'icon_name': 'recursos', 'icon_marker_color': '#442178'}

}
CATEGORIES = {
                'socialcurrency': {'name': _('Social Currencies'), 'icon_name': 'socialcurrency', 'icon_marker_color': '#800033'},
                'socialcurrency.inactive_network': {'name': _('Inactive Networks'), 'icon_name': 'socialcurrency', 'icon_marker_color': '#876d76'},
                'socialcurrency.active_network': {'name': _('Active Networks'), 'icon_name': 'socialcurrency', 'icon_marker_color': '#fe694b'},

                'activism': {'name': _('Activism'), 'icon_name': 'activism' , 'icon_marker_color': '#aa0000'},
                'activism.feminism': {'name': _('Feminism'), 'icon_name': 'feminism', 'icon_marker_color': '#442178'},
                'activism.humanrights': {'name': _('Human rights'), 'icon_name': 'humanrights', 'icon_marker_color': '#aa0000'},
                'activism.socialmov': {'name': _('Social movement'), 'icon_name': 'socialmov', 'icon_marker_color': '#aa0000'},
                'activism.ngo': {'name': _('NGO'), 'icon_name': 'ngo', 'icon_marker_color': '#aa0000'},
                'activism.merchandising': {'name': _('Merchandising'), 'icon_name': 'merchandising', 'icon_marker_color': '#aa0000'},

                'advising': {'name': _('Advising / consulting services'), 'icon_name': 'advising_consulting', 'icon_marker_color': '#483745'},
                'advising.architecture': {'name': _('Architecture, construction, restoration'), 'icon_name': 'architecture', 'icon_marker_color': '#483745'},
                'advising.insurance': {'name': _('Ethical insurance'), 'icon_name': 'insurance', 'icon_marker_color': '#800033'},
                'advising.legal': {'name': _('Legal and administrative services'), 'icon_name': 'legal', 'icon_marker_color': '#483745'},

                'nature': {'name': _('Nature'), 'icon_name': 'nature', 'icon_marker_color': '#445500'},
                'nature.animals': {'name': _('Animals'), 'icon_name': 'animals' , 'icon_marker_color': '#445500'},
                'nature.ecology': {'name': _('Ecologism'), 'icon_name': 'ecology', 'icon_marker_color': '#445500'},
                'nature.energy': {'name': _('Renewable Energy'), 'icon_name': 'energy', 'icon_marker_color': '#ff8600'},
                'nature.science': {'name': _('Science'), 'icon_name': 'science' , 'icon_marker_color': '#445500'},
                'nature.aventure': {'name': _('Adventure'), 'icon_name': 'aventure' , 'icon_marker_color': '#445500'},

                'bars': {'name': _('Bars & Restaurants'), 'icon_name': 'bars' , 'icon_marker_color': '#003380'},

                'culture': {'name': _('Culture, Arts & Entertainment'), 'icon_name': 'culture' , 'icon_marker_color': '#ff4b00'},
                'culture.museum': {'name': _('Art Gallery - Museum - Exposition'), 'icon_name': 'museum', 'icon_marker_color': '#ff4b00'},
                'culture.squat': {'name': _('Athenaeum - Squat'), 'icon_name': 'squat', 'icon_marker_color': '#ff4b00'},
                'culture.bookstore': {'name': _('Bookstore'), 'icon_name': 'bookstore', 'icon_marker_color': '#ff4b00'},
                'culture.cinema': {'name': _('Cinema'), 'icon_name': 'cinema', 'icon_marker_color': '#ff4b00'},
                'culture.dance': {'name': _('Dance'), 'icon_name': 'dance', 'icon_marker_color': '#ff4b00'},
                'culture.digitalarts': {'name': _('Digital Arts & graphic design'), 'icon_name': 'digitalarts', 'icon_marker_color': '#ff4b00'},
                'culture.audiovisual': {'name': _('Film and audiovisual'), 'icon_name': 'audiovisual', 'icon_marker_color': '#ff4b00'},
                'culture.library': {'name': _('Libraries'), 'icon_name': 'library', 'icon_marker_color': '#ff4b00'},
                'culture.music': {'name': _('Music & concert room'), 'icon_name': 'music', 'icon_marker_color': '#ff4b00'},
                'culture.painting': {'name': _('Painting'), 'icon_name': 'painting', 'icon_marker_color': '#ff4b00'},
                'culture.photo': {'name': _('Photography'), 'icon_name': 'photo', 'icon_marker_color': '#ff4b00'},
                'culture.sculpture': {'name': _('Sculpture'), 'icon_name': 'sculpture', 'icon_marker_color': '#ff4b00'},
                'culture.sport': {'name': _('Sport'), 'icon_name': 'sport', 'icon_marker_color': '#ff4b00'},
                'culture.theatre': {'name': _('Theatre'), 'icon_name': 'theatre', 'icon_marker_color': '#ff4b00'},
                'culture.toys': {'name': _('Toys'), 'icon_name': 'toys', 'icon_marker_color': '#ff4b00'},

                'community': {'name': _('Community, Networks, Commons & Free Knowledge'), 'icon_name': 'community' , 'icon_marker_color': '#aa0088'},
                'community.hacker': {'name': _('Hackerspaces'), 'icon_name': 'hacker' , 'icon_marker_color': '#aa0088'},
                'community.fablab': {'name': _('Fablabs'), 'icon_name': 'fablab', 'icon_marker_color': '#aa0088'},
                'community.garden': {'name': _('Community Gardens'), 'icon_name': 'garden', 'icon_marker_color': '#aa0088'},
                'community.associations': {'name': _('Neighborhood Associations'), 'icon_name': 'associations', 'icon_marker_color': '#aa0088'},

                'craft': {'name': _('Craft'), 'icon_name': 'craft' , 'icon_marker_color': '#552200'},
                'craft.ceramics': {'name': _('Ceramics'), 'icon_name': 'ceramics', 'icon_marker_color': '#552200'},
                'craft.natural': {'name': _('Natural Elements'), 'icon_name': 'natural', 'icon_marker_color': '#552200'},
                'craft.metal': {'name': _('Metal, Iron'), 'icon_name': 'metal', 'icon_marker_color':'#552200'},
                'craft.wood': {'name': _('Wood'), 'icon_name': 'wood', 'icon_marker_color': '#552200'},
                'craft.musical': {'name': _('Musical Instruments'), 'icon_name': 'musical', 'icon_marker_color': '#552200'},
                'craft.jewelry': {'name': _('Jewelry'), 'icon_name': 'jewelry', 'icon_marker_color': '#552200'},
                'craft.paper': {'name': _('Paper'), 'icon_name': 'paper', 'icon_marker_color': '#552200'},
                'craft.mineral': {'name': _('Mineral Stones'), 'icon_name': 'mineral', 'icon_marker_color': '#552200'},
                'craft.leather': {'name': _('Leather'), 'icon_name': 'leather', 'icon_marker_color': '#552200'},
                'craft.textil': {'name': _('Textile'), 'icon_name': 'textil', 'icon_marker_color': '#552200'},
                'craft.glass': {'name': _('Glass'), 'icon_name': 'glass', 'icon_marker_color': '#552200'},

                'clothes': {'name': _('Clothes, shoes ands accessories'), 'icon_name': 'clothes' , 'icon_marker_color': '#004455'},
                'clothes.kids': {'name': _('Baby & Kids'), 'icon_name': 'kids' , 'icon_marker_color': '#004455'},

                'communication': {'name': _('Communication & publishing'), 'icon_name': 'communication' , 'icon_marker_color': '#162d50'},
                'communication.media': {'name': _('Alternative Community Media'), 'icon_name': 'media' , 'icon_marker_color': '#162d50'},
                'communication.translation': {'name': _('Editing & Translation'), 'icon_name': 'translation' , 'icon_marker_color': '#162d50'},
                'communication.magazine': {'name': _('Magazines & Newspapers'), 'icon_name': 'magazine' , 'icon_marker_color': '#162d50'},

                'coop': {'name': _('Cooperatives'), 'icon_name': 'coop', 'icon_marker_color': '#002b11'}, #esta mejor no se podrá elegir como icono principal pero sí como filtro.

                'economy': {'name': _('Economy'), 'icon_name': 'economy', 'icon_marker_color': '#800033'},
                'economy.poe': {'name': _('FairCoin Point of Exchange PoE'), 'icon_name': 'poe', 'icon_marker_color': '#800033'},
                'economy.fairpay': {'name': _('FairPay'), 'icon_name': 'fairpay', 'icon_marker_color': '#800033'},
                'economy.banking': {'name': _('Ethical banking & Financing'), 'icon_name': 'banking', 'icon_marker_color': '#800033'},

                'education': {'name': _('Education, Training & Research'), 'icon_name': 'education', 'icon_marker_color': '#ff8600'},
                'education.languages': {'name': _('Languages'), 'icon_name': 'languages', 'icon_marker_color': '#ff8600'},
                'education.freeschool': {'name': _('Alternative schools'), 'icon_name': 'freeschool', 'icon_marker_color': '#ff8600'},
                'education.upbringing': {'name': _('Shared upbringing'), 'icon_name': 'upbringing', 'icon_marker_color': '#ff8600'},

                'event': {'name': _('Event'), 'icon_name': 'event', 'icon_marker_color': '#002b11'},

                'fairtrade': {'name': _('Fair Trade'), 'icon_name': 'fairtrade', 'icon_marker_color': '#002b11'},
                'faircoop_ln': {'name': _('FairCoop Local Nodes'), 'icon_name': 'faircoop', 'icon_marker_color': '#002b11'},

                'food': {'name': _('Food & Drink'), 'icon_name': 'food', 'icon_marker_color': '#002b11'},
                'food.bakery': {'name': _('Bakery'), 'icon_name': 'bakery', 'icon_marker_color': '#002b11'},
                'food.fruits': {'name': _('Fruits & Vegetables Store'), 'icon_name': 'fruits', 'icon_marker_color': '#002b11'},
                'food.others': {'name': _('Other Food'), 'icon_name': 'other_foods', 'icon_marker_color': '#002b11'},

                'health_care': {'name': _('Healthcare'), 'icon_name': 'health_care', 'icon_marker_color': '#442178'},
                'health_care.cosmetics': {'name': _('Cosmetics'), 'icon_name': 'cosmetics', 'icon_marker_color': '#442178'},
                'health_care.dentist': {'name': _('Dentist'), 'icon_name': 'dentist', 'icon_marker_color': '#442178'},
                'health_care.hair': {'name': _('Hair'), 'icon_name': 'hair', 'icon_marker_color': '#442178'},
                'health_care.manual': {'name': _('Manual & Natural Therapies'), 'icon_name': 'manual', 'icon_marker_color': '#442178'},
                'health_care.mediation': {'name': _('Facilitation & Mediation'), 'icon_name': 'mediation', 'icon_marker_color': '#442178'},
                'health_care.elderly': {'name': _('Elderly Care'), 'icon_name': 'elderly', 'icon_marker_color': '#442178'},
                'health_care.disabled': {'name': _('Functional Diversity People Attention'), 'icon_name': 'disabled', 'icon_marker_color': '#442178'},
                'health_care.childcare': {'name': _('Childcare'), 'icon_name': 'childcare', 'icon_marker_color': '#442178'},
                'health_care.medical': {'name': _('Medical Services'), 'icon_name': 'medical', 'icon_marker_color': '#442178'},

                'housing': {'name': _('Housing & Lodging'), 'icon_name': 'housing' , 'icon_marker_color': '#2d1650'},
                'housing.apartment': {'name': _('Apartment'), 'icon_name': 'apartment', 'icon_marker_color': '#2d1650'},
                'housing.b_and_b': {'name': _('Bed & Breakfast'), 'icon_name': 'b_and_b', 'icon_marker_color': '#2d1650'},
                'housing.camping': {'name': _('Camping'), 'icon_name': 'camping', 'icon_marker_color': '#2d1650'},
                'housing.hotel': {'name': _('Hotel'), 'icon_name': 'hotel', 'icon_marker_color': '#2d1650'},
                'housing.rent': {'name': _('Sell & Rent'), 'icon_name': 'rent', 'icon_marker_color': '#2d1650'},
                'housing.rural': {'name': _('Rural Hotel'), 'icon_name': 'rural', 'icon_marker_color': '#2d1650'},

                'second_hand': {'name': _('Second Hand & Barter'), 'icon_name': 'second_hand' , 'icon_marker_color': '#aa0000'},

                'supplies': {'name': _('Supplies'), 'icon_name': 'supplies' , 'icon_marker_color': '3000000'},
                'supplies.plumbing': {'name': _('Plumbing'), 'icon_name': 'plumbing', 'icon_marker_color': '#000000'},
                'supplies.hardware': {'name': _('Hardware'), 'icon_name': 'hardware', 'icon_marker_color': '#000000'},
                'supplies.electricity': {'name': _('Electricity'), 'icon_name': 'electricity', 'icon_marker_color': '#000000'},
                'supplies.carpentry': {'name': _('Carpentry'), 'icon_name': 'carpentry', 'icon_marker_color': '#000000'},
                'supplies.cleaning': {'name': _('Cleaning Products'), 'icon_name': 'cleaning', 'icon_marker_color': '#000000'},
                'supplies.restoration': {'name': _('Construction'), 'icon_name': 'restoration', 'icon_marker_color': '#000000'},
                'supplies.office': {'name': _('Office Products'), 'icon_name': 'office', 'icon_marker_color': '#000000'},
                'supplies.home': {'name': _('Home Products'), 'icon_name': 'home', 'icon_marker_color': '#000000'},

                'tech': {'name': _('Technology, electronics & Internet'), 'icon_name': 'tech' , 'icon_marker_color': '#000000'},
                'tech.software': {'name': _('Software'), 'icon_name': 'software' , 'icon_marker_color': '#000000'},

                'transport': {'name': _('Transport & Travel'), 'icon_name': 'transport' , 'icon_marker_color': '#003380'},
                'transport.garage': {'name': _('Garage'), 'icon_name': 'garage' , 'icon_marker_color': '#003380'},
                'transport.ev_charger': {'name': _('Electrical Vehicle Charger'), 'icon_name': 'ev_charger' , 'icon_marker_color': '#003380'},
                'transport.tourism': {'name': _('Responsable Tourism'), 'icon_name': 'tourism' , 'icon_marker_color': '#003380'},
                'transport.messagery': {'name': _('Delivery'), 'icon_name': 'messagery' , 'icon_marker_color': '#003380'},
                'transport.sharedcar': {'name': _('Shared vehicle'), 'icon_name': 'sharedcar' , 'icon_marker_color': '#003380'},
                'transport.storage': {'name': _('Circular Economy storage'), 'icon_name': 'storage' , 'icon_marker_color': '#003380'}
}
