from factory.django import DjangoModelFactory
import maplisting.models

from wagtail_factories import (
    PageFactory,
    ImageFactory
)
from factory import (
    Faker,
    SubFactory,
    LazyAttribute,
    Trait)
import random
from django.contrib.gis.geos import Point
from factory.fuzzy import BaseFuzzyAttribute


class FuzzyPoint(BaseFuzzyAttribute):
    def fuzz(self):
        #43.362343, -8.411540) A coruña
        #36.13300°N / 5.3417° gibraltar
        return Point(random.uniform(-8.411, -0.641),
                     random.uniform(37.133, 42.362))
        # return Point(random.uniform(-180.0, 180.0),
        #              random.uniform(-90.0, 90.0))
# from networkapi.highlights.factory import HighlightFactory
# from networkapi.news.factory import NewsFactory
#
sentence_faker  = Faker('sentence', nb_words=3, variable_nb_words=False)
header_faker  = Faker('sentence', nb_words=6, variable_nb_words=True)
description_faker = Faker('paragraphs', nb=6)

#
# class CTAFactory(DjangoModelFactory):
#     class Meta:
#         abstract = True
#         exclude = (
#             'header_text',
#             'description_text',
#         )
#
#     name = Faker('text', max_nb_chars=80)
#     header = LazyAttribute(lambda o: o.header_text.rstrip('.'))
#     description = LazyAttribute(lambda o: ''.join(o.description_text))
#     newsletter = Faker('word')
#
#     # Lazy Values
#     description_text = description_faker
#     header_text = header_faker
#
#
# class PetitionFactory(CTAFactory):
#     class Meta:
#         model = networkapi.wagtailpages.models.Petition
#
#
# class SignupFactory(CTAFactory):
#     class Meta:
#         model = networkapi.wagtailpages.models.Signup
#
from django.utils.text import slugify

# class MapListingPageFactory(PageFactory):
#     class Meta:
#         model = maplisting.models.MapListingPage
#
#     intro = Faker('text', max_nb_chars=140)
#     body = Faker('paragraph', nb_sentences=5, variable_nb_sentences=True)
#     title = LazyAttribute(lambda o: o.title_text.rstrip('.'))

#
class MyImageFactory(ImageFactory):
    title = Faker('sentence', nb_words=3, variable_nb_words=False)

class CMSPageFactory(PageFactory):
    class Meta:
        abstract = True


class MapListingPageFactory(CMSPageFactory):
    # intro = Faker('text', max_nb_chars=140)
    # body = Faker('paragraph', nb_sentences=5, variable_nb_sentences=True)
    title = LazyAttribute(lambda o: o.faker_locale.sentence())
    intro = LazyAttribute(lambda o: o.faker_locale.text())
    body = LazyAttribute(lambda o: o.faker_locale.paragraph())
    address = LazyAttribute(lambda o: o.faker_locale.address())
    # main_category =  LazyAttribute(lambda o: MapListingCategory.objects.order_by('?')[0])
    # main_image = SubFactory(MyImageFactory)
    location = FuzzyPoint()
    class Meta:
        exclude = ('faker_locale',)
        model = maplisting.models.MapListingPage


class MapListingTranslationPageFactory(CMSPageFactory):
    title = LazyAttribute(lambda o: o.faker_locale.sentence())
    intro = LazyAttribute(lambda o: o.faker_locale.text())
    body = LazyAttribute(lambda o: o.faker_locale.paragraph())

    class Meta:
        exclude = ('faker_locale',)
        model = maplisting.models.MapListingTranslationPage



#
# class CampaignPageFactory(CMSPageFactory):
#     class Meta:
#         model = networkapi.wagtailpages.models.CampaignPage
#
#     class Params:
#         no_cta = Trait(cta=None)
#
#     cta = SubFactory(PetitionFactory)
#
#
# class MiniSiteNameSpaceFactory(PageFactory):
#     class Meta:
#         model = networkapi.wagtailpages.models.MiniSiteNameSpace
#
#
# class PeoplePageFactory(PageFactory):
#     class Meta:
#         model = networkapi.wagtailpages.models.PeoplePage
#
#     title = 'people'
#
#
# class NewsPageFactory(PageFactory):
#     class Meta:
#         model = networkapi.wagtailpages.models.NewsPage
#
#     title = 'news'
#
#
# class StyleguideFactory(PageFactory):
#     class Meta:
#         model = networkapi.wagtailpages.models.Styleguide
#
#     title = 'styleguide'
#
#
# class InitiativesPageFactory(PageFactory):
#     class Meta:
#         model = networkapi.wagtailpages.models.InitiativesPage
#
#     title = 'initiatives'
#
#
# class ParticipatePageFactory(PageFactory):
#     class Meta:
#         model = networkapi.wagtailpages.models.ParticipatePage
#
#     title = 'participate'
#
#
# class OpportunityPageFactory(CMSPageFactory):
#     class Meta:
#         model = networkapi.wagtailpages.models.OpportunityPage
#
#     class Params:
#         no_cta = Trait(cta=None)
#
#     cta = SubFactory(SignupFactory)
#
#
# class FeaturedFactory(DjangoModelFactory):
#     class Meta:
#         abstract = True
#
#     page = SubFactory(WagtailHomepageFactory)
#
#
# class HomepageFeaturedNewsFactory(FeaturedFactory):
#     class Meta:
#         model = networkapi.wagtailpages.models.HomepageFeaturedNews
#
#     news = SubFactory(NewsFactory)
#
#
# class HomepageFeaturedHighlightsFactory(FeaturedFactory):
#     class Meta:
#         model = networkapi.wagtailpages.models.HomepageFeaturedHighlights
#
#     highlight = SubFactory(HighlightFactory)
