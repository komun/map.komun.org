from django.contrib.syndication.views import feedgenerator, Feed
from django.utils.encoding import iri_to_uri
from django.utils.translation import ugettext as _
from wagtail.core.models import PageRevision
from maplisting.utils import remove_embedded_videos
from maplisting.models.map_listing_page import MapListingPage
from maplisting.categories import CATEGORIES
from mapo.settings.base import BASE_URL


class ExtendedRSSFeed(feedgenerator.Rss201rev2Feed):
    """
    Create a type of RSS feed that has content:encoded elements.
    """
    def root_attributes(self):
        attrs = super(ExtendedRSSFeed, self).root_attributes()
        attrs['xmlns:content'] = 'http://purl.org/rss/1.0/modules/content/'
        return attrs

    def add_item_elements(self, handler, item):
        super(ExtendedRSSFeed, self).add_item_elements(handler, item)
        handler.addQuickElement(u'content:encoded', item['content_encoded'])


class LatestRevisionsFeed(Feed):
    title = 'Mapo moderation feeds'
    link = "/feed/admins/revisions/"
    description = "Revisions pending for moderation in mapo"
    # feed_type = ExtendedRSSFeed
    # description_template = "feeds/description_revisions.html"

    def items(self):
        return PageRevision.submitted_revisions.order_by('-created_at')[:10]

    def item_title(self, item):
        return item.page.title

    def item_author(self, item):
        return item.user

    def get_revision_link(self, item):
        return '{}/admin/pages/{}/revisions/'.format(BASE_URL, item.page.id)

    def item_description(self, item):
        return 'Cambios en página {}({}) fueron enviados para <a href="{}">moderación</a>.'.format(item.page.title, item.page.id, self.get_revision_link(item))

    def item_link(self, item):
        return self.get_revision_link(item)

    def item_pubdate(self, item):
        return item.created_at


class LatestEntriesFeed(Feed):
    title = 'New places Mapo Komun.org'
    link = "/feed/admins/published/"
    description = "Updates on additions in mapo"
    feed_type = ExtendedRSSFeed
    description_template = "feeds/description_admins.html"

    def items(self):
        return MapListingPage.objects.order_by('-first_published_at')[:10]

    def item_title(self, item):
        return item.title

    def item_author(self, item):
        return item.owner

    def item_guid(self, item):
        url = ('{}/?placeid={}'.format(BASE_URL, item.slug))
        return iri_to_uri(url)

    def item_pubdate(self, item):
        return item.first_published_at

    # def item_updateddate(self, item):
    #     """
    #     Takes an item, as returned by items(), and returns the item's
    #     updateddate.
    #     """
    def item_categories(self, item):
        return item.get_other_categories_slugs()

    def item_link(self, item):
        return('{}/?placeid={}'.format(BASE_URL, item.slug))
        # return reverse('news-item', args=[item.pk])

    def item_extra_kwargs(self, item):
        return {'content_encoded': '<h1>Item %d</h1><p>lorem ipsum...</p>' % item.id}

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        page = context['obj']
        context['base_url'] = BASE_URL
        map_icon = page.map_icon

        context['tags'] = page.tags.all().values_list('name', flat=True)

        context['body'] = remove_embedded_videos(str(page.body))
        if map_icon:
            context['icon_name'] = CATEGORIES[map_icon]['icon_name']
        if page.main_image:
            context['image_url_html'] = '<img src="{}{}">'.format(BASE_URL, page.main_image.get_rendition('width-300').url)
        else:
            context['image_url_html'] = ''
        return context
