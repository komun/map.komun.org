from django.apps import AppConfig


class MapListingConfig(AppConfig):
    name = 'maplisting'
