from wagtail.core import hooks
from django.urls import reverse
from django.utils.translation import ugettext as _


@hooks.register('construct_explorer_page_queryset')
def show_my_pages_only(parent_page, pages, request):
    if not (request.user.is_staff or request.user.is_superuser):
        pages = pages.filter(owner=request.user)

    return pages

@hooks.register('construct_image_chooser_queryset')
def show_my_uploaded_images_only(images, request):
    # Only show uploaded images
    if not (request.user.is_staff and request.user.is_superuser):

    # images = images.filter(collection=request.user) #TODO: get the images of the same collection only
        images = images.filter(uploaded_by_user=request.user)

    return images

from wagtail.admin import widgets as wagtailadmin_widgets

@hooks.register('register_page_listing_buttons')
def page_listing_buttons(page, page_perms, is_parent=False):
    page_root_id = 8
    url = '/admin/pages/{0}/add_subpage/'.format(page.id)
    # import pdb
    # pdb.set_trace()
    if not page.id == page_root_id:
        yield wagtailadmin_widgets.Button(
            _('Add translation'),
            reverse('wagtailadmin_pages:add_subpage', args=[page.id]),
            attrs={'title': _("Add a translation page to '{title}' ").format(title=page.get_admin_display_title())},
            classes={'button', 'button-small', 'bicolor', 'icon', 'white', 'icon-plus'},
            priority=50
        )
    else:

        url = '/admin/pages/{0}/add_subpage/'.format(page.id)

        yield wagtailadmin_widgets.Button(
            _('Add Map Point'),
            reverse('wagtailadmin_pages:add_subpage', args=[page.id]),
            attrs={'title': _("Add a map point to '{title}' ").format(title=page.get_admin_display_title())},
            classes={'button', 'button-small', 'bicolor', 'icon', 'white', 'icon-plus'},
            priority=5
        )
