import re
import json
import requests
import codecs

import os.path
# import pprint
from django.http import HttpResponse
from django.shortcuts import render
from django.utils.translation import activate as activate_translations, ugettext as _

from mapo.settings.base import BASE_URL, PROJECT_DIR
from maplisting.categories import CATEGORIES, CATEGORIES_PV

all_social_currency_points = None
activity_colours = ['#a8ce54', '#fdcc41', '#ff9955', '#fe694b']
activity_levels = [_('Low'), _('Medium'), _('High'), _('Very High')]
verylow_colour = '#35c6e1'
inactive_colour = '#876d76'
min_average = 0
max_average = 80
edge_colour = (max_average - min_average) / len(activity_colours)


def get_feature(lon, lat):
    return {"type": "Feature",
            "geometry": {
              "type": "Point",
              "coordinates": [
                float(lon), float(lat),
              ]
            },
            "properties": {}
    }

def get_social_currency_point_short_details(lang=None, slug=None):
    from maplisting.models.map_listing_index_page import MapListingIndexPage
    activate_translations(lang)
    # MapListingIndexPage.social_currency_data, MapListingIndexPage.social_currency_points = get_social_currency_points()
    if not MapListingIndexPage.social_currency_data:
        MapListingIndexPage.social_currency_data, MapListingIndexPage.social_currency_points = get_social_currency_points()


    details = {}
    # current_lang = (lang or get_language_from_path(request.path_info) or LANGUAGE_CODE)
    id = slug[len("SC_"):]
    data = MapListingIndexPage.social_currency_data

    if id in data:
        point_data = data[id]

        if 'comment' in point_data and point_data['comment'] and len(point_data['comment']) > 3:
            comment = point_data['comment']
        else:
            comment = _('LETS/Social currency network')
        if point_data:
            details['image_url'] = get_social_currency_logo(point_data)
            details['description'] = comment
            details['title'] = point_data['name']

    return details

def get_social_currency_logo(point_data):
    is_CES = point_data['platform'] == 'CES'
    is_AU_CES = point_data['platform'] == 'AU_CES'
    is_cForge = point_data['platform'] == 'CForge'
    is_iCES = point_data['platform'] == 'ICES'
    if 'logo' in point_data and point_data['logo']:
        picture_url = point_data['logo']
    else: picture_url = ""
    if is_CES and not picture_url:
        picture_url = '/statics/img/ceslogo.jpg'
    elif is_AU_CES and not picture_url:
        picture_url = '/statics/img/australian_ceslogo.jpg'
    elif is_iCES and not picture_url:
        picture_url = '/statics/img/integralceslogo.jpg'
    elif is_cForge and not picture_url:
        picture_url = 'http://seldebergues.communityforge.net/profiles/cforge/themes/sky_seldulac/logo.png'

    return picture_url

def show_gen_ecovillage_point(request, slug):
    from maplisting.models.map_listing_index_page import MapListingIndexPage

    if not MapListingIndexPage.ecovillage_data:
        MapListingIndexPage.ecovillage_data, MapListingIndexPage.ecovillage_points = get_ecovillage_points()

    id = slug[len("GEN_"):]
    data = MapListingIndexPage.ecovillage_data
    html = ''
    cat_dict = {'community.associations': {'name':'Ecovillage', 'icon_name': 'associations', 'icon_marker_color': '#aa0088'},
                'housing.rural': {'name': 'Rural', 'icon_name': 'rural', 'icon_marker_color': '#2d1650'},
                'nature.ecology': {'name':'Ecology', 'icon_name': 'ecology', 'icon_marker_color': '#445500'},
                'nature': {'name': 'Nature', 'icon_name': 'nature', 'icon_marker_color': '#445500'}
                }
    if id in data:
        point_data = data[id]
        title = point_data['title']
        url = 'https://ecovillage.org/wp-json/gen/v1/mapdata?type=gen_project&ID={}'.format(id)
        resp = requests.get(url=url)
        
        data = json.loads(resp.text)
        body = ''
        
        picture_url = 'https://ecovillage.org/wp-content/plugins/gen-custom/build/img/global-ecovillage-network-logo.png'
        if data and 'content' in data:
            body = data['content']

        if '/wp-content/uploads' in body:
            pattern = 'background-image: url\(/wp-content/uploads/(.+?)\)">'
            # extract a json from response to a class "dict"
            matches = re.findall(pattern, str(body))
            print(matches)
            if matches:
                img_file = matches[0]
                # print(img_file)
                picture_url = 'https://ecovillage.org/wp-content/uploads/{}'.format(img_file)


        html = render(request, 'maplisting/ecovillage_right_card.html', {
            'title': title,
            'body': body,
            'point': point_data,
            # 'join_url': join_url,
            'picture_url': picture_url,
            'categories': cat_dict,
            'tags': ["ecovillage", "ecoaldea", "GEN"]}
        )
    return HttpResponse(html, content_type='text/html')

def show_social_currency_point(request, lang, slug):
    activate_translations(lang)
    from maplisting.models.map_listing_index_page import MapListingIndexPage

    # MapListingIndexPage.social_currency_data, MapListingIndexPage.social_currency_points = get_social_currency_points()
    if not MapListingIndexPage.social_currency_data:
        MapListingIndexPage.social_currency_data, MapListingIndexPage.social_currency_points = get_social_currency_points()


    id = slug[len("SC_"):]
    data = MapListingIndexPage.social_currency_data
    html = ''
    if id in data:
        point_data = data[id]
        transactions_html = ''
        is_CES = point_data['platform'] == 'CES' or point_data['platform'] == 'AU_CES'
        is_AU_CES = point_data['platform'] == 'AU_CES'
        is_cForge = point_data['platform'] == 'CForge'
        is_iCES = point_data['platform'] == 'ICES'

        if is_iCES or is_CES:
            title = '{}: {}'.format(id, point_data['name'])
        else:
            title = '{}'.format(point_data['name'])


        if 'comment' in point_data and point_data['comment'] and len(point_data['comment']) > 2:
            comment = point_data['comment']
        else: comment = _("LETS/Social currency network")

        if is_CES:
            if is_AU_CES:
                platform = 'Australian CES'
                join_url = 'https://www.communityexchange.net.au/docs/join2.asp?xid={}'.format(id)
            else:
                platform = 'Community Exchange System (CES)'
                join_url = 'https://www.community-exchange.org/docs/join2.asp?xid={}'.format(id)


            transactions_html += '<p><b>'+_('Created on:')+'</b> {}'.format(point_data['created_date'])
            transactions_html += '<br><b>NID</b>: {}'.format(point_data['NID'])
        elif is_iCES:
            platform = 'integralCES'
            join_url = 'https://integralces.net/es/user/register/{}'.format(id)

        elif is_cForge:
            platform = 'Community Forge'
            join_url = point_data['join_url']
        else:
            # import pdb
            # pdb.set_trace()
            platform = point_data['platform']
            join_url = point_data['join_url']

        picture_url = get_social_currency_logo(point_data)
        if not join_url.startswith('http'):
            join_url = 'http://{}'.format(join_url)

        intro = comment
        if 'currency' in point_data:
            transactions_html += '<br><b>'+_('Currency name:')+'</b> {}'.format(point_data['currency'])
        if point_data['active']:
            if is_CES:
                amount = 0
                transactions_html += '<br><b>'+_('Last months:')+'</b><ul>'
                transactions = point_data['transactions']
                for t in transactions:
                    transactions_html += ('<li><b>{}</b>:'+_('transactions:')+' {} '+_('total:')+' {} u.</li>').format(t['month'], t['num_tx'],
                                                                                          t['amount_tx'])
                    amount += float(t['num_tx'])
                average = amount / len(transactions)
                if average < 3:
                    level_colour = verylow_colour
                    level_name = 'Very low'
                else:
                    network_level = min(int(average / edge_colour), len(activity_levels) - 1)
                    # print(amount, edge_colour, network_level)
                    level_colour = activity_colours[network_level]
                    level_name = _(activity_levels[network_level])
                transactions_html += '</ul></p>'
            elif is_iCES:
                #Integral CES
                total_accounts = int(point_data['total_accounts'])
                if total_accounts < 4:
                    level_colour = verylow_colour
                    level_name = _('Very low')
                else:
                    network_level = min(int(total_accounts / 33), len(activity_levels)-1)
                    level_colour = activity_colours[network_level]
                    level_name = _(activity_levels[network_level])
            elif is_cForge:
                #Community Forge .csv
                year_transactions = int(point_data['year_transactions'])
                total_accounts = int(point_data['active_members'])
                if year_transactions < 3:
                    level_colour = verylow_colour
                    level_name = _('Very low')
                else:
                    network_level = min(int(year_transactions / edge_colour), len(activity_levels) - 1)
                    level_colour = activity_colours[network_level]
                    level_name = _(activity_levels[network_level])
            else:
                level_colour = activity_colours[-2]
                level_name =_( activity_levels[-2])

        else:
            # Inactive network
            level_colour = inactive_colour
            level_name = 'Inactive'
            if 'last_month' in point_data:
                transactions_html += '<br><b>'+_('No activity since:')+'</b> {}'.format(point_data['last_month'])

        if is_CES:
            transactions_html += '<br><b>'+_('Monthly average transactions:')+'</b> {}<br><b>Monthly average amount</b>: {}'.format(
                point_data['num_tx_average'],
                point_data['amount_average'])
        elif is_iCES:
            transactions_html += '<br><b>'+_('Total accounts:')+'</b> {}'.format(point_data["total_accounts"])
        elif is_cForge:
            transactions_html += ('<br><b>'+_('Active members:')+'</b>: {}<br><b>'+_('Yearly transactions:')+'</b>: {}').format(
                point_data["active_members"], point_data["year_transactions"])
        transactions_html += '<br><b>'+_('Activity level:')+' <font color="{}">{}</font></b></p>'.format(level_colour,
                                                                                               level_name)

        html = render(request, 'maplisting/social_currency_right_card.html', {
            'intro': intro, 'title': title, 'body': transactions_html,
            'point': point_data,
            'platform': platform,
            'join_url': join_url,
            'picture_url': picture_url,
            # 'categories': cat_dict,
            'tags': ["social currency", "monedas sociales", "CES"]}
        )
    return HttpResponse(html, content_type='text/html')


def get_social_currency_points():
    path = os.path.join(PROJECT_DIR, 'CES_social_currencies.json')
    aus_ces_path = os.path.join(PROJECT_DIR, 'AU_CES_social_currencies.json')
    ices_path = os.path.join(PROJECT_DIR, 'ices_social_currencies.json')
    cf_path = os.path.join(PROJECT_DIR, 'community_forge_social_currencies.json')
    json_points = []
    sc_points = {}
    with open(path) as json_data:
        sc_points = json.load(json_data)
    with open(aus_ces_path) as json_data:
        new_points = json.load(json_data)
        sc_points.update(new_points)
    with open(ices_path) as json_data:
        new_points = json.load(json_data)
        sc_points.update(new_points)
    with open(cf_path) as json_data:
        new_points = json.load(json_data)
        sc_points.update(new_points)


    update_path = os.path.join(PROJECT_DIR, 'update_social_currencies.json')
    with open(update_path, encoding='utf8') as update_json_data:
        update_sc_points = json.load(update_json_data)
        for code, updated_details in update_sc_points.items():
            if 'delete' in updated_details:
                sc_points.pop(code)
                continue
            if code in sc_points:
                sc_points[code].update(updated_details)
            else:
                sc_points[code] = updated_details

    no_coords = 0
    todelete = []
    for id, network in sc_points.items():
        if not ('lat' in network and network['lat']):
            no_coords += 1
        else:
            is_CES = network["platform"] == "CES"
            is_AU_CES = network['platform'] == 'AU_CES'
            is_iCES = network["platform"] == "ICES"
            is_cForge = network['platform'] == 'CForge'
            if is_CES and int(network['num_tx_average']) < 2:
                todelete.append(id)
                continue
            json_p = get_feature(network['lon'], network['lat'])
            if is_iCES or is_CES or is_AU_CES:
                title = '{}: {}'.format(id, network['name'])
            else:
                title = '{}'.format(network['name'])

            json_p['properties']['name'] = '{}'.format(title)
            json_p['properties']['slug'] = 'SC_{}'.format(id)

            if network['active']:
                json_p['properties']["categories"] = {'socialcurrency.active_network': {'name': _('Active Social Currency Network'),
                                                      'icon_name': 'socialcurrency'}}

                if is_CES or is_AU_CES:
                    amount = 0
                    transactions = network['transactions']
                    for t in transactions:
                        amount += float(t['num_tx'])
                    average = amount / len(transactions)
                    if average < 3:
                        level_colour = verylow_colour
                        level_name = _('Very low')
                    else:
                        network_level = min(int(average / edge_colour), len(activity_levels)-1)
                        level_colour = activity_colours[network_level]
                        level_name = _(activity_levels[network_level])
                elif is_iCES:
                    #Integral CES
                    total_accounts = int(network['total_accounts'])
                    if total_accounts < 4:
                        level_colour = verylow_colour
                        level_name = _('Very low')
                    else:
                        network_level = min(int(total_accounts / 33), len(activity_levels)-1)
                        level_colour = activity_colours[network_level]
                        level_name = _(activity_levels[network_level])
                elif is_cForge:
                    #Community Forge .csv
                    year_transactions = int(network['year_transactions'])
                    # total_accounts = int(network['active_members'])
                    if year_transactions < 3:
                        level_colour = verylow_colour
                        level_name = _('Very low')
                    else:
                        network_level = min(int(year_transactions / edge_colour), len(activity_levels) - 1)
                        level_colour = activity_colours[network_level]
                        level_name = _(activity_levels[network_level])
                else:
                    level_colour = activity_colours[-2]
                    level_name = _(activity_levels[-2])

            else:
                #Inactive network
                json_p['properties']["categories"] = {'socialcurrency.inactive_network': {'name': 'Inactive Social Currency Network',
                                                      'icon_name': 'socialcurrency'}}
                level_colour = inactive_colour
                level_name = _('Inactive')

            if is_cForge:
                content_html = ""
            else:
                content_html = '<br><b>'+_('Currency name:')+'</b> {}'.format(network['currency'])
            content_html += '<br><b>'+_('Activity level:')+' <font color="{}">{}</font></b></p>'.format(level_colour, level_name)
            if 'comment' in network and network['comment'] and len(network['comment']) > 3:
                comment = network['comment']
            else:
                comment = _('LETS/Social currency network')

            json_p['properties']['description'] = '{}<br>{}'.format(
                                                   comment, content_html)
            json_p['properties']['icon_name'] = 'socialcurrency'
            json_p['properties']['icon_marker_color'] = level_colour
            if not network['active']:
                json_p['properties']['icon_disabled'] = False

            json_points.append(json_p)
    for i in todelete:
        sc_points.pop(i)

    return sc_points, json_points


def get_ecovillage_points():
    from maplisting.gen_ecovillages import points as ecovillage_points

    json_points = []
    sc_points = {}
    print('Imported ', len(ecovillage_points), 'GEN points')

    for p in ecovillage_points:
        json_p = get_feature(p['lng'], p['lat'])
        id = p['ID']
        title = p['post_title']

        json_p['properties']['name'] = '{}'.format(title)
        json_p['properties']['slug'] = 'GEN_{}'.format(id)
        json_p['properties']["categories"] = {'community.associations': {'name': 'Neighborhood Associations',
                                              'icon_name': 'associations'}}
        json_p['properties']['description'] = 'GEN Ecovillage'
        json_p['properties']['icon_name'] = 'associations'
        json_p['properties']['icon_marker_color'] = '#aa0088'
        sc_points[id] = {'title': title}

        json_points.append(json_p)
    return sc_points, json_points


def get_sobirania_points():
    # from maplisting.sobirania_points import points as sobirania_points
    import json
    # json.load('/home/alberto/workspace/faircoop/komun/mapo/mapkomun/maplisting/')
    path = os.path.join(PROJECT_DIR, 'sobirania_points.json')
    json_points = []
    ln_data = {}
    f = codecs.open(path, encoding='utf-8')
    doc = f.read()
    # doc = f.read().replace("\\u","u")
    # doc = doc.replace("\\/", "/")
    sobirania_points = json.loads(doc)
    json_points = []
    sc_points = {}
    print('Imported ', len(sobirania_points), 'Sobirania points')

    for p in sobirania_points:
        json_p = get_feature(p['lon'], p['lat'])
        slug = p['slug']
        title = p['name']
        category = p['category']
        location = p.get('location', None)
        description = p.get('description', '')
        comarca = p.get('comarca', None)
        email = p.get('email', None)
        stamp = p.get('stamp', None)
        contact = p.get('contact', None)

        json_p['properties']['name'] = '{}'.format(title)
        json_p['properties']['slug'] = 'PV_{}'.format(slug)
        json_p['properties']["categories"] = {category: CATEGORIES_PV[category]}
        json_p['properties']['description'] = CATEGORIES_PV[category]['name']
        json_p['properties']['icon_name'] = category
        json_p['properties']['icon_marker_color'] = CATEGORIES_PV[category]['icon_marker_color']
        sc_points[slug] = {'title': title,
                           'category': category,
                           'description': p['description'],
                           'location': location,
                           'comarca': comarca,
                           'email': email,
                           'stamp': stamp,
                           'contact': contact
                          }

        json_points.append(json_p)
    return sc_points, json_points


def show_sobirania_point(request, slug):
    from maplisting.models.map_listing_index_page import MapListingIndexPage

    if not MapListingIndexPage.sobirania_points:
        MapListingIndexPage.sobirania_data, MapListingIndexPage.sobirania_points = get_sobirania_points()

    id = slug[len("PV_"):]
    data = MapListingIndexPage.sobirania_data
    html = ''
    if id in data:
        point_data = data[id]
        print(point_data)
        category = point_data['category']
        picture_url = None
        # if not picture_url:
        #     picture_url = '/statics/img/faircoop-logo.png'
        cat_dict = {} #'faircoop_ln': {'name': 'Faircoop Local Node', 'icon_name': 'faircoop'}}
        cat_dict[category] = CATEGORIES_PV[category]
        tags = [] #"FairCoop", "Local node"]

        description = point_data['description'] #.replace('\/','/')

        html = render(request, 'maplisting/sobirania_right_card.html', {
            'title': point_data['title'],
            'body': description,
            'point': point_data,
            'picture_url': picture_url,
            'categories': cat_dict,
            'location': point_data['location'],
            'stamp': point_data['stamp'],
            'contact': point_data['contact'],
            'email': point_data['email'],
            'comarca': point_data['comarca'],
            'tags': tags}
        )
    return HttpResponse(html, content_type='text/html')


def show_fc_local_node_point(request, slug):
    from maplisting.models.map_listing_index_page import MapListingIndexPage

    if not MapListingIndexPage.fc_local_nodes_points:
        MapListingIndexPage.fc_local_nodes_data, MapListingIndexPage.fc_local_nodes_points = get_fc_local_nodes_points()

    id = slug[len("FC_"):]
    data = MapListingIndexPage.fc_local_nodes_data
    html = ''
    if id in data:
        point_data = data[id]
        picture_url = point_data['icon']
        if not picture_url:
            picture_url = '/statics/img/faircoop-logo.png'
        cat_dict = {'faircoop_ln': {'name': 'Faircoop Local Node', 'icon_name': 'faircoop'}}
        projects = point_data['projects_involved']
        tags = ["FairCoop", "Local node"]
        for p in projects.split("#"):
            p = p.replace(",","").strip()
            if p:
                tags.append(p)

        description = point_data['description'].replace('\/','/')
        point_data['assembly_notes'] = point_data['assembly_notes'].replace('\/','/')
        html = render(request, 'maplisting/fc_local_node_right_card.html', {
            'name': point_data['name'],
            'body': description,
            'point': point_data,
            'picture_url': picture_url,
            'categories': cat_dict,
            'tags': tags}
        )
    return HttpResponse(html, content_type='text/html')


def get_fc_local_nodes_points():
    path = os.path.join(PROJECT_DIR, 'FCLN.json')
    json_points = []
    ln_data = {}
    f = codecs.open(path, encoding='utf-8')
    doc = f.read().replace("\\u","u")
    doc = doc.replace("\\/", "/")
    ln_points = json.loads(doc)
    # with open(path) as json_data:
    #     ustr_to_load = unicode(json_data.read(), 'latin-1')


    print('Imported ', len(ln_points), 'LN points')

    for p in ln_points:
        json_p = get_feature(p['longitude'], p['latitude'])
        id =  p['id'].replace(".", "_")
        name = p['name']

        json_p['properties']['name'] = '{}'.format(name)
        json_p['properties']['slug'] = 'FC_{}'.format(id)
        json_p['properties']["categories"] = {'faircoop_ln': {'name': 'Faircoop Local Node',
                                              'icon_name': 'faircoop'}}
        json_p['properties']['description'] = p['description']
        json_p['properties']['icon_name'] = 'faircoop'
        json_p['properties']['icon_marker_color'] = '#aa0088'
        ln_data[id] = p

        json_points.append(json_p)
    return ln_data, json_points

def get_preferred_langs(lang):
    preferred_langs = {
    'ca': ['ca', 'es', 'en'],
    'gl': ['gl', 'pt', 'es', 'en'],
    }
    if lang in preferred_langs:
        return preferred_langs[lang]
    elif lang != 'en':
        return [lang, 'en']
    else:
        return ['en']


def get_url_from_point(point, lang='en'):
    # lon = point.location.coords[0]
    # lat = point.location.coords[1]
    #url = '{}/?l={}&lat={}&lon={}&zoom=15&id={}'.format(BASE_URL, lang, lat, lon, point.slug)
    url = '/?l={}&id={}'.format(lang, point.slug)

    return url


def get_embed_url_with_parameters(body):
    if 'youtube.com' in body or 'youtu.be' in body:
        regex = re.compile(r"(?:https:\/\/)?(?:www\.)?(?:youtube\.com|youtu\.be)\/(?:watch\?v=)?(.+?)", re.MULTILINE)
        body = regex.sub(r"https://www.youtube.com/embed/\1", body)

    if 'vimeo.com' in body:
        regex = re.compile(r'vimeo.com/(.+?)"', re.MULTILINE)
        body = regex.sub(r'player.vimeo.com/video/\1?&portrait=0&badge=0"', body)

    return body


def transform_embedded_videos(body):
    body = get_embed_url_with_parameters(body)
    pattern = r'<embed embedtype="media" url="(.+?)"(.*?)/>'
    regexp = re.compile(pattern, re.MULTILINE)
    body = regexp.sub(r'<iframe frameborder="0" width="250" src="\1" webkitallowfullscreen mozallowfullscreen allowfullscreen></iframe>', body)
    return body


def remove_embedded_videos(body):
    body = re.sub(r'<embed embedtype="media" url="(.+?)"/>', r'', body)
    return body
