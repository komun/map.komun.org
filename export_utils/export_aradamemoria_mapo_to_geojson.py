import requests
from lxml import html
from bs4 import BeautifulSoup
import json
import os

MAPO_URL = "https://mapa.aradamemoria.org"
GEOJSON_FILE = "/tmp/arada.geojson"
ORIGIN_MEDIA_URL = "https://mapa.aradamemoria.org/media/images/"
DOWNLOAD_TMP = "/tmp/aradatmp/"
FINAL_MEDIA_URL = "https://mapo.komun.org/media/other_mapos/aradamemoria/"
ICON_URL = "https://mapo.komun.org/statics/img/categories/feminism_black.svg"

def obtener_puntos_mapa(url):
    # Realizar la solicitud GET a la URL
    response = requests.get(url)

    # Verificar si la solicitud fue exitosa (código de respuesta 200)
    if response.status_code == 200:
        # Obtener el contenido JSON de la respuesta
        json_data = response.json()

        # Procesar el JSON y extraer la información necesaria
        puntos_dict = {}
        total = 0
        for punto in json_data['response']:
            total += 1
            nombre = punto['properties']['name']
            slug = punto['properties']['slug']
            coordenadas = punto['geometry']['coordinates']

            # Agregar la información al diccionario utilizando el slug como clave única
            if slug in puntos_dict:
                if coordenadas != puntos_dict[slug]["coordenadas"]:
                    slug = slug + str(total)
                else:
                    a = {'nombre': nombre, 'slug': slug, 'coordenadas': coordenadas}
                    print('Repetido mismas coords', a)
                
            puntos_dict[slug] = {'nombre': nombre, 'slug': slug, 'coordenadas': coordenadas}

        print('totales', total)
        return puntos_dict
    else:
        # Si la solicitud no fue exitosa, imprimir el código de respuesta
        print(f"Error al obtener la respuesta. Código de respuesta: {response.status_code}")
        return None

def obtener_contenido_punto(url_punto, nombre):
    response = requests.get(url_punto)

    # Verificar si la solicitud fue exitosa (código de respuesta 200)
    if response.status_code == 200:
        # Obtener el contenido HTML de la respuesta
        html = response.content.decode('utf-8')
        # print(html)
        soup = BeautifulSoup(html, 'html.parser')

        full_email = ""
        # Find the position of email-related strings
        email_start = html.find("<i class='fas fa-envelope fa-lg'></i>")
        email_end = html.find("<i class='fas fa-at fa-lg'></i>")

        # Extract the email address and domain
        if email_start != -1 and email_end != -1:
            email_icon_length = len("<i class='fas fa-envelope fa-lg'></i>")
            email_address = html[email_start + email_icon_length:email_end].strip()
            email_domain = html[email_end + len("<i class='fas fa-at fa-lg'></i>"):].strip()
            email_domain = email_domain.split('\n')[0].strip()
            full_email = f"{email_address}@{email_domain}"
            

        # Extraer la URL de la imagen
        img_element = soup.find('img', id='rightCard-img')
        img_url = img_element['src'] if img_element else None

        urls = []

        start_marker = 'href="'
        end_marker = '"'

        while start_marker in html:
            start_index = html.find(start_marker)
            end_index = html.find(end_marker, start_index + len(start_marker))

            if start_index != -1 and end_index != -1:
                url = html[start_index + len(start_marker):end_index]
                if  not url.startswith('/?id') and not url.startswith('https://www.openstreetmap.org'): 
                    urls.append(url)
                html = html[end_index:]

        # print("Extracted URLs:", urls)
        return {'email': full_email, 'urls': urls, 'img_url': img_url}
    else:
        # Si la solicitud no fue exitosa, imprimir el código de respuesta
        print(f"Error al obtener la respuesta. Código de respuesta: {response.status_code}")
        return None

url_servicio_web = f"{MAPO_URL}/map_points/?l=es"
# Obtener los puntos del mapa
puntos_mapa = obtener_puntos_mapa(url_servicio_web)
print('TOTAL', len(puntos_mapa))


for slug, info in puntos_mapa.items():
    print(f"\n\nSlug: {slug}, Nombre: {info['nombre']}, Coordenadas: {info['coordenadas']}")
    url_punto = f"{MAPO_URL}/showMapPoint/lang/es/slug/{slug}/"

    # Obtener el contenido del punto
    contenido_punto = obtener_contenido_punto(url_punto, nombre=info['nombre'])
    # Imprimir el contenido del punto
    if contenido_punto:
        print(contenido_punto)
    puntos_mapa[slug]["email"] = contenido_punto['email']
    puntos_mapa[slug]["img_url"] = contenido_punto['img_url']
    puntos_mapa[slug]["urls"] = contenido_punto['urls']


#Descargar las imagenes si no existen
# Ensure the DOWNLOAD_TMP folder exists
if False: #No descargamos la imagenes
    os.makedirs(DOWNLOAD_TMP, exist_ok=True)
    for slug, info in puntos_mapa.items():
        if 'img_url' in info:
            image_url = info['img_url']
            if image_url:
                image_filename = os.path.basename(image_url)
                local_path = os.path.join(DOWNLOAD_TMP, image_filename)

                if not os.path.exists(local_path):
                    image_url = ORIGIN_MEDIA_URL + image_filename
                    response = requests.get(image_url)

                    if response.status_code == 200:
                        with open(local_path, 'wb') as f:
                            f.write(response.content)
                        print(f"Downloaded: {image_filename}")
                    else:
                        print(f"Failed to download: {image_filename}")
                else:
                    print(f"Already exists: {image_filename}")

#Exportando .geojson en /tmp
geojson = {
"type": "FeatureCollection",
"features": []
}
count = 0
for slug, punto in puntos_mapa.items():
    print('punto', punto)
    html = ''
    
    if 'img_url' in punto and punto['img_url'] and not '/None' in punto['img_url']:
        image_filename = os.path.basename(punto['img_url'])
        image_filename = image_filename.replace('.jpeg','.jpg')
        image_filename = image_filename.replace('.png','.jpg')
        image_filename = image_filename.replace('.width-300','')
        image_url = FINAL_MEDIA_URL + image_filename
        html = f'{{{{{image_url}}}}}'
    if 'email' in punto:
        email = punto['email']
        if email:
            html += f'\nCorreu: **{email}**\n'
    
    if 'urls' in punto and len(punto['urls']):
        html += '\nEnllaços:\n' 
        for url in punto['urls']:
            html += f'• [[{url}]]\n'

    feature = {
        "type": "Feature",
        "properties": {
            "_umap_options": {
                "color": "Yellow",
                "iconUrl": f"{ICON_URL}",
            },
            "name": punto['nombre'],
            "description": html
        },
        "geometry": {
            "type": "Point",
            "coordinates": punto['coordenadas']
        }
    }
    geojson["features"].append(feature)

# Guardar el GeoJSON en un archivo
with open(GEOJSON_FILE, 'w', encoding='utf-8') as f:
    json.dump(geojson, f, ensure_ascii=False, indent=2)
