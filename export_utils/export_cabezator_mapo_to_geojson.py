import requests
from lxml import html
from bs4 import BeautifulSoup
import json

MAPO_URL = "https://mapo.komun.org/cabezator"
GEOJSON_FILE = "/tmp/cabezator.geojson"
ICON_URL = "https://mapo.komun.org/statics/img/categories/health_care_black.svg"

def obtener_puntos_mapa(url):
    # Realizar la solicitud GET a la URL
    response = requests.get(url)

    # Verificar si la solicitud fue exitosa (código de respuesta 200)
    if response.status_code == 200:
        # Obtener el contenido JSON de la respuesta
        json_data = response.json()

        # Procesar el JSON y extraer la información necesaria
        puntos_dict = {}
        total = 0
        for punto in json_data['response']:
            total += 1
            nombre = punto['properties']['name']
            slug = punto['properties']['slug']
            coordenadas = punto['geometry']['coordinates']

            # Agregar la información al diccionario utilizando el slug como clave única
            if slug in puntos_dict:
                if coordenadas != puntos_dict[slug]["coordenadas"]:
                    slug = slug + str(total)
                else:
                    a = {'nombre': nombre, 'slug': slug, 'coordenadas': coordenadas}
                    print('Repetido mismas coords', a)
                
            puntos_dict[slug] = {'nombre': nombre, 'slug': slug, 'coordenadas': coordenadas}

        print('totales', total)
        return puntos_dict
    else:
        # Si la solicitud no fue exitosa, imprimir el código de respuesta
        print(f"Error al obtener la respuesta. Código de respuesta: {response.status_code}")
        return None

def obtener_contenido_punto(url_punto, nombre):
    # Realizar la solicitud GET a la URL del punto
    response = requests.get(url_punto)

    # Verificar si la solicitud fue exitosa (código de respuesta 200)
    if response.status_code == 200:
        # Obtener el contenido HTML de la respuesta
        html = response.content.decode('utf-8')

        # Utilizar BeautifulSoup para analizar el HTML
        soup = BeautifulSoup(html, 'html.parser')

        # Encontrar el elemento con id "rightCard-text"
        right_card_text = soup.find('div', {'id': 'rightCard-text'})

        # Verificar si se encontró el elemento
        if right_card_text:
            # Encontrar el enlace al PDF
            pdf_link = right_card_text.find('a', {'href': lambda x: x and 'pdf' in x})

            # Eliminar el enlace al PDF si existe
            if pdf_link:
                pdf_link.decompose()

            # Obtener el contenido del elemento con las etiquetas HTML
            contenido_texto = str(right_card_text)
            contenido_texto = contenido_texto[len('<div class="card-text" id="rightCard-text">'):]
            return contenido_texto
        else:
            print("No se encontró el elemento 'rightCard-text' en la página.")
            return None
    else:
        # Si la solicitud no fue exitosa, imprimir el código de respuesta
        print(f"Error al obtener la respuesta. Código de respuesta: {response.status_code}")
        return None

url_servicio_web = f"{MAPO_URL}/map_points/?l=es"
# Obtener los puntos del mapa
puntos_mapa = obtener_puntos_mapa(url_servicio_web)
print('TOTAL', len(puntos_mapa))

# Imprimir el diccionario resultante
if puntos_mapa:
    for slug, info in puntos_mapa.items():
        print(f"\n\nSlug: {slug}, Nombre: {info['nombre']}, Coordenadas: {info['coordenadas']}")
        url_punto = f"{MAPO_URL}/showMapPoint/lang/es/slug/{slug}/"
    
        # Obtener el contenido del punto
        contenido_punto = obtener_contenido_punto(url_punto, nombre=info['nombre'])
        puntos_mapa[slug]["html"] = contenido_punto
        # Imprimir el contenido del punto
        if contenido_punto:
            print(contenido_punto)

    #Exportando .geojson en /tmp
    geojson = {
    "type": "FeatureCollection",
    "features": []
    }
    count = 0
    for slug, punto in puntos_mapa.items():
        if 'html' in punto:
            count += 1
            feature = {
                "type": "Feature",
                "properties": {
                    "_umap_options": {
                        "color": "Yellow",
                        "iconUrl": f"{ICON_URL}",
                    },
                    "name": punto['nombre'],
                    "description": punto['html']
                },
                "geometry": {
                    "type": "Point",
                    "coordinates": punto['coordenadas']
                }
            }
            geojson["features"].append(feature)

    print('Total features with html', count)
    # Guardar el GeoJSON en un archivo
    with open(GEOJSON_FILE, 'w', encoding='utf-8') as f:
        json.dump(geojson, f, ensure_ascii=False, indent=2)
