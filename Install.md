https://www.howtoforge.com/tutorial/ubuntu-postgresql-installation/
sudo apt install python3-pip  apache2 libapache2-mod-wsgi-py3
sudo apt-get install libjpeg-dev libfreetype6-dev zlib1g-dev libtiff-dev
python3 -m pip install --user --upgrade pip
python3 -m pip install --user virtualenvwrapper
sudo apt-get install virtualenvwrapper  

add in .bashrc
source /usr/share/virtualenvwrapper/virtualenvwrapper.sh
alias python=python3

mkvirtualenv wagtailp3 -p /usr/bin/python3

#download the code directory through git or FTP
cd <codedirectory>
pip install -r requirements.txt
if error
export LANGUAGE=en_US.UTF-8
(wagtailp3) map@106:~/mapo$ export LANG=en_US.UTF-8
(wagtailp3) map@106:~/mapo$ export LC_ALL=en_US.UTF-8
-bash: warning: setlocale: LC_ALL: cannot change locale (en_US.UTF-8)
(wagtailp3) map@106:~/mapo$ export LC_CTYPE="en_US.UTF-8"
(wagtailp3) map@106:~/mapo$ locale-gen en_US.UTF-8

https://techjourney.net/convert-postgresql-template0-template1-encoding-to-utf8-sql_ascii-incompatible/

#Making compatible the wordpress passwords.
from django.contrib.auth.models import User
from django.contrib.auth.hashers import get_hasher
hasher = get_hasher('phpass')
for user in User.objects.filter(is_superuser=False):
    user.password = hasher.from_orig(user.password)
    user.save()

from django.contrib.auth.models import User
root = User.objects.filter(is_superuser=True).first()
for user in User.objects.filter(is_superuser=False):
    if user.password.startswith('phpass'):
        for p in user.owned_pages.all():
            p.owner = root
            p.save()
        for i in user.image_set.all():
            i.uploaded_by_user = root
            i.save()
        user.delete()

from wagtail.images.models import Image

sacar bbdd,
sudo su - postgres
pg_dump mapo > mapo.sql
mv wagtail.sql /tmp/
exit

#subir por ftp a /tmp
sudo su - postgres
dropdb mapo
createdb  -T template0 mapo
psql mapo < /tmp/mapo.sql
exit
sudo service apache2 restart

http://grimnes.no/blog/how-easily-deploy-wagtail-website/

#if you have to rename your bbdd name in psql
ALTER DATABASE usefaircoinwagtail RENAME TO mapo;