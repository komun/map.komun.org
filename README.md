
# Mapo install instructions

## install postgis and python3
https://www.howtoforge.com/tutorial/ubuntu-postgresql-installation/

    sudo apt install python3-pip  apache2 libapache2-mod-wsgi-py3
    sudo apt-get install libjpeg-dev libfreetype6-dev zlib1g-dev libtiff-dev
    python3 -m pip install --user --upgrade pip
    python3 -m pip install --user virtualenvwrapper


## create virtual environment

    sudo apt-get install virtualenvwrapper
    mkvirtualenv wagtailp3 -p /usr/bin/python3
    workon wagtailp3
    git clone git@gitlab.com:komun.org/map.komun.org.git
    cd mapkomun
    pip install -r requirements.txt

## configure db
modify DATABASES in mapkomun/mapo/settings/base.py
to point to your postgis database(you can create it using pgadmin3)
    python manage.py createsuperuser
    python manage.py migrate

## run in localhost:8000

    python manage.py runserver
