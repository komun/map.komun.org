��    �      �  K  ,      X     Y     m     �     �     �     �     �     �                    *  $   H     m  �   }     8  	   H     R     q     u     �     �     �  	   �  '   �  !   �          *     6     =     P  	   `     j     r  	   z  
   �     �  	   �     �     �     �     �  .   �           @     [  -   m     �     �     �     �  	   �     �     �     �                    $     -     5     S  #   k  	   �     �  ,   �      �     �          $     1     L     X  5   ^     �     �     �     �     �  
   �     �          *     ?     G  	   X     b     k     �     �     �     �     �  %   �     �     	               #     (  
   1     <     I     N     \     b     t  b   �  ;   �           &     /     A     M  0   f     �     �     �     �     �     �       	             :  �   G       !   &  	   H     R     [     a     t     �     �     �     �  	   �  	   �  
   �     �     �     �     	            $      B      W      k   /   o      �      �      �      �      �      �   
   �   <   !     B!  
   R!     ]!     n!     !     �!     �!     �!     �!     �!     �!     �!     �!  	   �!  $   "     ,"     5"     ="     E"     M"     T"     c"     k"     �"     �"     �"  !   �"     �"     �"     �"     �"     �"     #  	   3#     =#  
   I#  	   T#     ^#  	   f#     p#     w#  2   �#     �#     �#     �#     �#     �#  )   $     1$     C$     S$     \$     b$     i$     p$     �$     �$  "   �$     �$  +  �$  5   �&  %   !'     G'     O'     n'     ~'     �'     �'  	   �'  	   �'     �'     �'     �'     �'  .   �'  �   (  i   �(     1)     P)     c)     j)  	   x)  �  �)     +  -   %+     S+     \+     y+     �+     �+     �+  	   �+     �+     �+  #   �+  -   ,     B,  �   U,     -     ,-     5-     L-  1   Q-     �-     �-     �-  
   �-  *   �-  "   �-  $   	.     ..  
   ?.     J.     ].  	   s.     }.  	   �.     �.  
   �.  	   �.  	   �.     �.     �.  !   �.     �.  ;   /     A/     \/     x/  3   �/     �/     �/     �/     �/  
   �/  	   	0  	   0     0     :0     I0     N0     V0     i0     r0     �0     �0  
   �0     �0  ,   �0      1     #1  $   81     ]1     l1     �1     �1  Y   �1     2      2     42     A2     I2     b2     o2     �2     �2     �2     �2  	   �2  
   �2     �2     �2     3     3     ,3     B3  ,   [3     �3     �3     �3     �3     �3  	   �3     �3     �3     �3     �3     4     4     )4  q   64  @   �4     �4     �4     �4     	5     5  8   25     k5     s5     |5     �5     �5     �5     �5  	   �5     �5     6  k   $6     �6     �6     �6  	   �6     �6     �6     �6     
7     7     !7     >7  
   M7     X7     h7     y7     �7     �7     �7     �7      �7     �7     �7     8  .   8     >8  
   P8     [8     q8     u8     �8  	   �8  ?   �8     �8     �8     9     9     49     M9     ]9     e9     k9  #   �9  %   �9     �9  
   �9     �9  *   �9  
   :      :  
   2:     =:     D:     L:     ]:     f:     �:     �:     �:  "   �:     �:     �:     �:  	   ;  1   ;     G;  	   [;     e;     q;  
   �;     �;  	   �;     �;     �;  5   �;     �;     �;     <     <     -<  0   ?<     p<     �<     �<     �<     �<     �<     �<     �<  	   �<  #   �<     =    =  ;   .?  "   j?     �?  '   �?     �?     �?     �?     �?     �?     
@  	   @     @     )@     /@  1   D@  �   v@  q   DA  #   �A     �A     �A     �A     B     <      k   V   4       	   /   u       �              �       �   �       �   s       �   j   8   w      �           :       �   �   �      �   �       �               �           z   M   
   =   �   3           �       �   �   �   U   T   �   �   �   �       r          �   ]   P   �   �   �   �       �   a   0      �   �       �   �      �   �   �   �           x   +   f   �   �   X   ;   �   h         �   �   C       O   [   �       �           �   �   .   ~                   �   !   g   H   �   e   �           &       �   �   d   \       $       �       �      �   �   �   F   S       �   D       �   �   #   �       m   �      �       �   �   �      �   `   �   Y   B          �      Z       �           �      �         �   N       �       �   y       t   �   �   �   ,       �      )       �               �   �              �   �   �   p                              �      E   �   7   o   1   �   %   2       �          �                  G   '   -   �       �   �       L   �   K   (   �   i   �   *               �             �       _      }           ^       A   J   {   �          >      n   l       5   �       �   �   I   W   Q   �   �      �   �       �   6   R       "       9   �   �           |   @           b   �   �   �   �   q   �   v       ?   �           c    250 characters max. A comma-separated list of tags. Accepts: Account activation failed Activate account at Active Networks Active Social Currency Network Active members: Activism Activity level: Add Map Point Add a map point to '{title}'  Add a translation page to '{title}'  Add translation Add your Fediverse account (e.g. Mastodon), is much better than FB or Twitter. Check out our <a target="_blank" href="https://hostux.social/web/accounts/68683">account</a> and follow us! Address copied. Adventure Advising / consulting services All Alternative Community Media Alternative currencies Alternative schools Animals Apartment Architecture, construction, restoration Art Gallery - Museum - Exposition Athenaeum - Squat Baby & Kids Bakery Bars & Restaurants Bed & Breakfast Bookstore Camping Caption Carpentry Categories Ceramics Childcare Cinema Circular Economy storage Claim this place to edit it! Cleaning Products Click the link below to activate your account. Clothes, shoes ands accessories Communication & publishing Community Gardens Community, Networks, Commons & Free Knowledge Construction Contact info Cooperatives Coordinates Cosmetics Craft Created on: Culture, Arts & Entertainment Currency name: Dance Delete Delivery Dentist Digital Arts & graphic design Do you accept FairCoin? Do you accept LETS/social currency? Ecologism Economy Editing %(page_type)s <span>%(title)s</span> Editing %(page_type)s: %(title)s Editing & Translation Education, Training & Research Elderly Care Electrical Vehicle Charger Electricity Email Email with password reset instructions has been sent. Ethical banking & Financing Ethical insurance Event Fablabs Facilitation & Mediation Fair Trade FairCoin Point of Exchange PoE FairCoin donations to: FairCoop Local Nodes FairPay Faircoin Address Fediverse Feminism Film and audiovisual Filters Find location in map Food & Drink Forgot password Fruits & Vegetables Store Functional Diversity People Attention Gallery images Garage Glass Hackerspaces Hair Hardware Healthcare Hello there! High Home Products Hotel Housing & Lodging Human rights If affirmative, tell us which one do you accept, (e.g. EXSV, ECOS, Wir franc, Bristol pound, etc.) If you don't want to show your public address, leave empty. Image Inactive Inactive Networks Information Invalid FairCoin address It will be automatically resized to 300px width. Jewelry Join Join this network! LETS / Social Currency Network LETS / Social Currency: LETS/Social currency network Language Languages Last modified: %(last_mod)s Last months: Learn more about <a target="_blank" href="https://en.wikipedia.org/wiki/Local_exchange_trading_system">LETS</a> or <a target="_blank" href="https://en.wikipedia.org/wiki/Local_currency">local social currencies</a>. Leather Legal and administrative services Libraries Location Login Logo or main image Long description Low Magazines & Newspapers Manual & Natural Therapies Map Icon Map Komun Map Point Map Points Medical Services Medium Merchandising Metal, Iron Mineral Stones Monthly average transactions: Music & concert room Musical Instruments NGO Name of your LETS/Local social currency network Natural Elements Nature Neighborhood Associations New New %(page_type)s No activity since: Not member Not publicly displayed, only to locate the point in the map. Office Products Other Food Other categories Page Translation Page language Page locked Painting Paper Password changed Password reset failed Password reset successfully Phone Photography Platform: Please add the international prefix. Plumbing Preview Privacy Private Public Public Address Publish Publish this revision Publishing… Redirecting... Register Registration is currently closed. Renewable Energy Replace current draft Reset Reset it Reset password at %(site_name)s Responsable Tourism Revisions Rural Hotel Save draft Saving… Science Sculpture Search Second Hand & Barter Select several categories by holding the CTRL key. Sell & Rent Settings Shared upbringing Shared vehicle Short description Show my FairCoin address QR for donations Social Currencies Social movement Software Sport Status Submit Submit for moderation Supplies Tags Technology, electronics & Internet Textile The <b>Komun Mapo</b> is a collaborative map which gives a global overview of various initiatives which strive towards a <b>Commons Economy</b>: places and services which accept alternative currencies, FairCoin, Points of Exchange, local nodes, social currencies, eco-networks, food groups, fair B&Bs, product routes and shared vehicles... <a href='https://komun.org' target='_blank'>Visit our main page!</a> Search, find, share and add yourself! Contribute to generate alternatives which transform our surroundings through the <b>Integral Revolution</b>. The above link is valid for %(expiration_days)s days. The main icon to be shown in the map. Theatre This page has unsaved changes. Total accounts: Toys Translated tags Transport & Travel Unpublish Very High Very low Website Wood Yearly transactions: You are now registered. Activation email sent. You can learn more about FairCoin, download a free wallet <a target="_blank" href="https://komun.org/en/blog/faircoin-conceptos-fundamentales">here</a> and then add your address! You can login <a href='/accounts/login/?next=/admin/pages/add/maplisting/maplistingpage/8/'>here</a> now. Your account is now activated. by %(modified_by)s total: transactions: {} points Project-Id-Version: 
Report-Msgid-Bugs-To: 
POT-Creation-Date: 2019-02-14 20:01+0000
PO-Revision-Date: 2019-02-08 14:44+0100
Last-Translator: 
Language-Team: LANGUAGE
Language: zxx
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Plural-Forms: nplurals=2; plural=(n != 1);
X-Loco-Source-Locale: es_ES
X-Loco-Parser: loco_parse_po
X-Generator: Poedit 1.8.7.1
 250 caracters max. Una llista de etiquetes separades amb coma(,) Accepta: Activació de compte fallida Activa el compte a Xarxes actives Xarxes Actives Membres actius: Activisme Nivell d'activitat: Afegeix un punt de Mapa Afegeix un punt de mapa a '{title}' Afegeix una pàgina de traducció a '{title}' Afegeix traducció Afegeix el vostre Fediverse compte (p. ex. Mastodon), és molt millor que FB o Twitter. Visita el nostre <a target="_blank" href="https://hostux.social/web/accounts/68683">compte</a> i segueix-nos! Adreça copiada. Aventura Serveis de consultoria Tots Mitjans de comunicació comunitaris i alternatius Monedes alternatives Escoles lliures Animals Apartament Arquitectura, construcció, rehabilitació Galeria d'art - Museu - Exposició Ateneus - Okupes – Centres Socials Nadons i infants Forn de pa Bars i restaurants Llit i esmorzar - B&B Llibreria Càmping Subtítol Fusteria Categories Ceràmica Guarderia Cinema Magatzem d'Economia Circular Demana aquest punt per editar-lo! Neteja Feu clic a l'enllaç següent per activar el vostre compte. Roba, sabates i accessoris Comunicació i publicacions Jardins comunitaris Comunitat, xarxes, bens comuns i coneixement lliure Construcció Informació de contacte Cooperatives Coordenades Cosmètics Artesania Creat el: Cultura, art i entreteniment Nom de moneda: Ball Elimina Enviaments/Entrega Dentista Arts Digitals & disseny gràfic Acceptes FairCoin? Accepta moneda social/LETS? Ecologisme Economia Editant %(page_type)s <span>%(title)s</span> Editant %(page_type)s: %(title)s Edició i traducció Educació, formació i investigació Cura d'ancians Carregador de vehicle elèctric Electricitat Correu electrònic Un correu electrònic amb instruccions de restauració de la contrasenya ha estat enviat. Banca i finances ètiques Assegurança Ètica Esdeveniment Fablabs Facilitació i mediació Comerç Just Punt d'Intercanvi FairCoin PoE Donacions FairCoin a: Nodes Locals FairCoop FairPay Adreça FairCoin Fediverse Feminismes Cinema i audiovisual Filtres Troba ubicació al mapa Menjar i beure Recuperar contrasenya Fruiteries - Verduleries Atenció a persones amb diversitat funcional Galeria d' imatges Taller mecànic Vidre Hackerspaces Perruqueria - Cabell  Maquinari Salut i cures Hola! Alt Productes domèstics Hotel Allotjament i habitatge Drets Humans En cas afirmatiu, digue-nos quina moneda social accepta (per exemple, EXSV, ECOS, franc Wir, Bristol pound, etc.) Si no voleu mostrar la vostra adreça pública, deixeu-la buida. Imatge Inactiu Xarxes Inactives Informació Adreça de FairCoin invalida Serà automàticament redimensionat a 300px  de amplada. Joieria Uneix-te Uneix-te a aquesta xarxa! LETS / Xarxa de Moneda Social LETS / Moneda Social LETS / Xarxa de Moneda Social Llengua Llengües Ultim modificat: %(last_mod)s Ultim mesos: Qu son les <a target="_blank" href="https://ca.wikipedia.org/wiki/Moneda_local">monedes socials locals</a>? Cuir Serveis legals i administratius Biblioteques Ubicació Iniciar sessió Logotip o imatge principal Descripció llarga Baix Revistes i diaris Teràpies manuals i naturals Icona del mapa Mapa Komun Punt en el mapa Punts en el mapa Serveis Mèdics Medi Merchandising Metall, ferro Pedres Minerals Mitjana de transaccions mensual: Sala de música i concerts Instruments musicals NGO Nom de la vostra Xarxa de Moneda Social / LETS Elements naturals Naturalesa Associacions veïnals Nou Nou %(page_type)s Sense activitat desde: No membre No apareixerà públicament, només per situar el punt al mapa. Productes d'oficina Altres aliments Altres categories Pàgina de traducció Llenguatge de la pàgina Pàgina tancada Pintura Paper Contrasenya canviada Restabliment de contrasenya fallida Restabliment de contrasenya amb èxit Telèfon Fotografia Plataforma: Per favor afegeix el prefix internacional. Fontaneria Previsualització Privacitat Privat Públic Adreça Pública Publicar Publicar aquesta revisió Publicant... Redirigint... Registrar-se La inscripció és actualment clos Energia renovable Reemplaça esborrany actual Reinicialització Restablir Reinicialització de contrasenya  a %(site_name)s Turisme responsable Revisions Hotel Rural Salva esborrany Salvant... Ciència Escultura Buscar Segona mà i troc Selecciona diverses categories prement la tecla CTRL. Venda i lloguer Opcions Educació compartida Vehicle compartit Descripció curta Mostrar la meua adreça FairCoin per a donacions Monedes Socials Moviment Social Software/Programació Esport Estatus Enviar Enviar per moderació Subministraments Etiquetes Tecnologia, electrònica i Internet Tèxtil El <b>Mapa Komun</b> és un mapa col·laboratiu que ofereix una visió global de diverses iniciatives que s'esforcen per aconseguir una <b>economia comuna*</b>: llocs que accepten FairCoin, oficines de canvi (POEs), nodes locals de FairCoop, monedes de la comunitat, Ecoxarxes, grups de consum, rutes i vehicles compartits.... <a href='https://komun.org' target='_blank'>Visita la pàgina!</a> Buscar, trobar, compartir i contribuir a generar alternatives que transformen el nostre entorn a través de la <b>Revolució Integral</b>. L'enllaç anterior és vàlid per %(expiration_days)s dies. La icona que es mostrarà al mapa. Teatre Aquesta pàgina té canvis no guardats. Comptes totals: Joguines Etiquetes traduïdes Transport i viatges Despublicar Molt Alt Molt baix Pàgina web Fusta Transaccions anuals: Tu ets ara registrat. E-mail d'activació enviat. Podeu obtenir més informació sobre FairCoin, descarregar una cartera gratuïta <a target="_blank" href="https://komun.org/en/blog/faircoin-conceptos-fundamentales"> aquí </a> i afegir la vostra adreça! Pots iniciar sessió <a href='/accounts/login/?next=/admin/pages/add/maplisting/maplistingpage/8/'>aquí</a> ara. El vostre compte està ara activat. per %(modified_by)s total: transaccions: {} punts 