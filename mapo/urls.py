from django.conf import settings
from django.conf.urls import include, url
from django.contrib import admin
from django.conf.urls.i18n import i18n_patterns
from django.views.i18n import JavaScriptCatalog

from wagtail.admin import urls as wagtailadmin_urls
from wagtail.core import urls as wagtail_urls
from maplisting import views
# from wagtail.documents import urls as wagtaildocs_urls

from maplisting.feeds import LatestEntriesFeed, LatestRevisionsFeed

urlpatterns = [
    url(r'^django-admin/', admin.site.urls),

    url(r'^admin/', include(wagtailadmin_urls)),
    # url(r'^documents/', include(wagtaildocs_urls)),
    url(r'^accounts/', include('registration.backends.default.urls')), #Registration with email confirmation
    # url(r'^accounts/', include('registration.backends.simple.urls')), #Simple registration without email confirmation
    # url(r'^accounts/', include('registration.backends.activation.urls')),
    # url(r'^accounts/', include('django.contrib.auth.urls')),
    #TODO: add the searchPoints generic url here.
    # url(r'^/$', views.frontend_view, name='index'),
    url(r'^feed/admin/published/$', LatestEntriesFeed()),
    url(r'^feed/admin/revisions/$', LatestRevisionsFeed()),
    url('^i18n/', include('django.conf.urls.i18n')),
    url(r'^jsi18n/$', JavaScriptCatalog.as_view(), name='javascript_catalog'),


    url(r'', include(wagtail_urls)),
    # url(r'^search/$', search_views.search, name='search'),

    # For anything not caught by a more specific rule above, hand over to
    # Wagtail's page serving mechanism. This should be the last pattern in
    # the list:


    # Alternatively, if you want Wagtail pages to be served from a subpath
    # of your site, rather than the site root:
    # (r'^dir/', include(wagtail_urls)),
    # urlpatterns += static(r'/', document_root=settings.FRONTEND_ROOT)

]
# urlpatterns += i18n_patterns(
#     # These URLs will have /<language_code>/ appended to the beginning
#     url(r'^search/$', search_views.search, name='search'),
#     url(r'^dir/', include(wagtail_urls)),
#     # url(r'', include(wagtail_urls)),
# )


if settings.DEBUG:
    from django.conf.urls.static import static
    from django.contrib.staticfiles.urls import staticfiles_urlpatterns
    import os.path
    FRONTEND_URL = '/statics/'
    FRONTEND_ROOT = os.path.join(settings.BASE_DIR, 'mapo/templates/frontend/statics')

    # Serve static and media files from development server
    urlpatterns += staticfiles_urlpatterns()
    urlpatterns += static(settings.MEDIA_URL, document_root=settings.MEDIA_ROOT)
    urlpatterns += static(FRONTEND_URL, document_root=FRONTEND_ROOT)
