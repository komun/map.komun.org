from .base import *

# SECURITY WARNING: don't run with debug turned on in production!
DEBUG = True

# SECURITY WARNING: keep the secret key used in production secret!
SECRET_KEY = 'otk0c6$&m=1-e9t$!q9tkyz@__e9%sslk8vcs3%d^a#=+yiw+1'

# SECURITY WARNING: define the correct hosts in production!
ALLOWED_HOSTS = ['*']

# EMAIL_BACKEND = 'django.core.mail.backends.console.EmailBackend'
EMAIL_BACKEND = 'django.core.mail.backends.smtp.EmailBackend'
BASE_URL = 'https://mapo.komun.org'
# try:
#     from .local import *
# except ImportError:
#     pass
