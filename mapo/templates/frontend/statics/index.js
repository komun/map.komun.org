var allitems = []; // array with all the items before filtering
var items = []; // the items showed in the map, after the categories
var categories = {};
var sorted_category_slugs = [];
var filters = [];
var allCheckbox = {checked: true};

function copyToClipboard(element) {
  var $temp = $("<input>");
  $("body").append($temp);
  $temp.val(element).select();
  document.execCommand("copy");
  $temp.remove();
  $('#alert_copied').show();
}

function hexToRGBA(hex,opacity){
    hex = hex.replace('#','');
    r = parseInt(hex.substring(0,2), 16);
    g = parseInt(hex.substring(2,4), 16);
    b = parseInt(hex.substring(4,6), 16);

    result = 'rgba('+r+','+g+','+b+','+opacity+')';
    return result;
}
function printFiltersList() {
  var html = "";

  sorted_category_slugs.forEach(function(category){
    if (categories.hasOwnProperty(category)) {
      if(category.indexOf('.') !== -1) div_class = 'lvl2';
      else div_class = 'lvl1';
//      div_class = 'lvl1';
      html += `<div class='`+div_class+`'>
                <label><input id="check` + category + `" onclick="updateFilter('`+category+`')" type='checkbox' value='' checked='`+categories[category].checked+`' >
                  <img style='width:30px;height:30px;
                  border: 2px solid `+categories[category].icon_marker_color+`;'
                  class="marker"
                  src='/statics/img/categories/` + categories[category].icon_name + `_black.svg' />
                  `+categories[category].name+`</label>
              </div>`;
    }
  });

  document.getElementById("filtersList").innerHTML = html;
}
function getItem(id) {
  for(var i=0; i<items.length; i++) {
    if(items[i].properties.slug==id) {
      return(items[i]);
    }
  }
  return("not found");
}

function showRightCard(slug) {
  $.get("/showMapPoint/lang/"+lang+"/slug/"+slug+"/", function(data){
    rightCard = data;
    document.getElementById("rightCard").innerHTML = rightCard;
  });

  document.getElementById("rightCard").className+=" rightCard-show";
}

function hideRightCard() {
  document.getElementById("rightCard").className ="card";
}

function turnLoadingBar(state){
    loadingbar_html = `
        <div class="cssload-loader-inner">
            <div class="cssload-cssload-loader-line-wrap-wrap">
                <div class="cssload-loader-line-wrap"></div>
            </div>
            <div class="cssload-cssload-loader-line-wrap-wrap">
                <div class="cssload-loader-line-wrap"></div>
            </div>
            <div class="cssload-cssload-loader-line-wrap-wrap">
                <div class="cssload-loader-line-wrap"></div>
            </div>
            <div class="cssload-cssload-loader-line-wrap-wrap">
                <div class="cssload-loader-line-wrap"></div>
            </div>
            <div class="cssload-cssload-loader-line-wrap-wrap">
                <div class="cssload-loader-line-wrap"></div>
            </div>
        </div>
    `
    if(state === true) document.getElementById("loadingbar").innerHTML=loadingbar_html;
    else document.getElementById("loadingbar").innerHTML="";
}
function updateMarkers(id_to_popup) {
  var list_markers = {};
  markers.clearLayers();
  for(var i=0; i<items.length; i++) {
    var lat = items[i].geometry.coordinates[1];
    var lon = items[i].geometry.coordinates[0];
    if(items[i].properties.hasOwnProperty("icon_disabled")){
        var opacity_decimal = 0.5
        var opacity_integer = 50
    }else{
        var opacity_decimal = 1
        var opacity_integer = 100
    }

    var icon = L.divIcon({
        className: 'markerInvisible',
        popupAnchor:  [10, 0], // point from which the popup should open relative to the iconAnchor
        html: `<img style='opacity: `+opacity_decimal+`; filter: alpha(opacity=`+opacity_integer+`); width:30px;height:30px;border: 2px solid `+items[i].properties.icon_marker_color+`;
        box-shadow: 0 0 5px 4px ` + hexToRGBA(items[i].properties.icon_marker_color, 0.2) + `
          ;' class='marker' src='/statics/img/categories/` + items[i].properties.icon_name + `_black.svg' />`
    });
    var marker = L.marker([lat, lon], {icon: icon});
    marker.id = items[i].properties.slug;
    if(items[i].properties.description!=null) {
      marker.bindPopup("<b>"+items[i].properties.name+"</b><br><i>"+items[i].properties.description+"</i>");
    } else {
      marker.bindPopup("<b>"+items[i].properties.name+"</b>");
    }
    marker.on('mouseover', function(e) {
      this.openPopup();
    });
    marker.on('mouseout', function(e) {
      this.closePopup();
    });
    marker.on('click', function(e) {
      var item = getItem(this.id);
      placeid = this.id;
      var lat = item.geometry.coordinates[1];
      var lon = item.geometry.coordinates[0];
      var zoom = 15;
      showRightCard(this.id);
      window.history.pushState({},"", "?id=" + placeid);
    });
    markers.addLayer(marker);
    list_markers[marker.id] = marker;
    if(id_to_popup != undefined && marker.id == id_to_popup){
        showInMap(items[i]);
    }

  }
  map.addLayer(markers);

  if(id_to_popup != undefined){
      if(list_markers.hasOwnProperty(id_to_popup)){
          m = list_markers[id_to_popup];

          markers.zoomToShowLayer(m, function() {
              if (!m._icon) m.__parent.spiderfy();
              m.openPopup();
          });
      }
  }

  return list_markers

}
function updateFilter(selectedFilter) {
  turnLoadingBar(true);
  category = categories[selectedFilter];

  if(category.checked == undefined || category.checked===true) {
    category.checked = false;
    document.getElementById('check' + selectedFilter).checked = false;
  } else {
    category.checked = true;
    document.getElementById('check' + selectedFilter).checked = true;
  }
  if(selectedFilter.indexOf('.') === -1){ selectChildrenCheckboxes(selectedFilter, category.checked)}
  setTimeout(function() {
        applyFilters();
        turnLoadingBar(false);
    }, 0);

}

function clickAllFilters(){
    turnLoadingBar(true);
    setTimeout(function() {
        selectAllFilters();
        turnLoadingBar(false);
    }, 0);

}
function selectAllFilters(empty) {
    if(empty !== undefined || allCheckbox.checked===true) {
      items = [];
      allCheckbox.checked = false;
      document.getElementById('allCheckbox').checked = false;
      for (var property in categories) {
          categories[property].checked=false;
          document.getElementById('check' + property).checked = false;
      }
    } else {
      items = JSON.parse(JSON.stringify(allitems));
      allCheckbox.checked= true;

      for (var property in categories) {
          categories[property].checked=true;
          document.getElementById('check' + property).checked = true;
        }
      document.getElementById('allCheckbox').checked = true;
    }
    list_markers = updateMarkers();
}

function itemInItems(array, el) {
  for(var i=0;i<array.length; i++) {
    if(array[i].properties.slug===el.properties.slug) {
      return true;
    }
  }
  return false;
}

function selectChildrenCheckboxes(filter, status){
    for (var property in categories) {
        if(property.indexOf(filter) !== -1){ //if is a subcheckbox contains the name of the parent category
            if (categories[property].checked == undefined || categories[property].checked!==status) {
                categories[property].checked = status;
                document.getElementById('check' + property).checked = status;
            }
        }
    }
}

function applyFilters() {
  items = [];
  for(var i=0; i<allitems.length; i++) {
    for (var property in allitems[i].properties.categories) {
      if (allitems[i].properties.categories.hasOwnProperty(property)) {
          if(categories[property].checked == undefined || categories[property].checked===true) {
            // check if item is not yet in items array
            if(itemInItems(items, allitems[i])===false) {
              items.push(JSON.parse(JSON.stringify(allitems[i])));
            }
          }
        }
      }
  }
  list_markers = updateMarkers();
}
function changeLanguage() {
  lang = document.getElementById("lang").value;
  window.history.pushState({},"", "?l="+lang+"&lat=" + lat + "&lon=" + lon + "&zoom=" + zoom);
  location.reload();
}
function updateLanguage(lang) {
  document.getElementById("lang").value = lang;
}
function printSearchResults(items) {
  var html = "";
  html += "<div class='list-group'>";
  for(var i=0; i<items.length; i++) {
    html += `
      <div onclick="goToItem('` + items[i].properties.slug + `')" class="list-group-item list-group-item-action flex-column align-items-start">
        <div class="d-flex w-100 justify-content-between">
          <h5 class="mb-1">` + items[i].properties.name + `</h5>
          <small>3 days ago</small>
        </div>`;
      if(items[i].properties.description!=null) {
        html += `<p class="mb-1">` + items[i].properties.description + `</p>`;
      }
      html += `
      </div>
    `;
  }
  html += "</div>";
  document.getElementById("searchResults").innerHTML = html;
}
function search() {
  var searchResults = [];
  var key = document.getElementById("searchinput").value.toUpperCase();
  if(key=="") {
    document.getElementById("searchResults").innerHTML = "";
    return;
  }
  for(var i=0; i<allitems.length; i++) {
    if(allitems[i].properties.name.toUpperCase().includes(key)) {
      searchResults.push(allitems[i]);
    }
  }
  printSearchResults(searchResults);
}
function showInMap(item) {
  lat = item.geometry.coordinates[1];
  lon = item.geometry.coordinates[0];
//  window.history.pushState({},"", "?l="+lang+"&lat=" + lat + "&lon=" + lon + "&zoom=" + zoom);
  map.setView([lat, lon], 16);
  window.history.pushState({},"", "?id=" + item.properties.slug);
}
function showPopup(slug){
    if(list_markers.hasOwnProperty(slug)){
      m = list_markers[slug];

      markers.zoomToShowLayer(m, function() {
          if (!m._icon) m.__parent.spiderfy();
          m.openPopup();
      });
  }
}
function goToItem(id) {
  var item = getItem(id);
  showRightCard(id);
  showInMap(item);
  showPopup(id);

}

function checkInitialFilters(param_filters){
    if(param_filters == 'monedasocial' || param_filters == 'socialcurrency' || param_filters == 'sel'){
        param_filters = ['socialcurrency', 'socialcurrency.inactive_network', 'socialcurrency.active_network'];
    }
    if (param_filters){
        turnLoadingBar(true);
        if( typeof param_filters === 'string' ) {
            param_filters = [ param_filters ];
        }
        setTimeout(function() {
            selectAllFilters(false);
            for (var i in param_filters) {
                filter = param_filters[i];
                if (categories.hasOwnProperty(filter)) {
                    document.getElementById('check' + filter).checked = true;
                    categories[filter].checked = true;
                }
            }

            applyFilters();
            turnLoadingBar(false);
        }, 0);


      }

}
// -----
// init
// -----

// get url parameters
var url = new URL(window.location.href);

var lang = url.searchParams.get("lang");
if(lang==undefined) {
  lang = url.searchParams.get("l");
}
var lat = url.searchParams.get("lat");
var lon = url.searchParams.get("lon");
var zoom = url.searchParams.get("zoom");
var placeid = url.searchParams.get("placeid");
var param_filters = url.searchParams.get("filters");
if(placeid==undefined){
    placeid = url.searchParams.get("id");
}


if((lat==undefined)||(lon==undefined)) {
  lat = 16.80454;
  lon = 11.95313;
}
if(zoom==undefined) {
  zoom=2;
}
if(lang==undefined) {
  if (language!=undefined){
        lang = language;
        window.history.pushState({},"", "?l="+lang+"&lat=" + lat + "&lon=" + lon + "&zoom=" + zoom);
  }else{
        lang = "en";
  }
}
updateLanguage(lang);

tiles_url = 'https://maps.wikimedia.org/osm-intl/{z}/{x}/{y}.png?lang='+lang

var tiles = L.tileLayer(tiles_url, {
    maxZoom: 18,
    attribution: '<a target="_blank" href="https://komun.org/'+lang+'/source-code-and-collaborations">'+gettext('Source code and collaborations')+'</a>. Map data &copy; OpenStreetMap contributors'
});

var map = L.map('map', {layers: [tiles]});
map.setView([lat, lon], zoom);
// alert(lat+" "+lon+" "+zoom+" ")
map.addEventListener('dragend', function(ev) {
  var coord = map.getCenter();
   lat = Math.round(coord.lat * 100000) / 100000;
   lon = Math.round(coord.lng * 100000) / 100000;
   window.history.pushState({},"", "?l="+lang+"&lat=" + lat + "&lon=" + lon + "&zoom=" + zoom);
});
map.on('zoomend', function(ev) {
    zoom = ev.target._zoom;
    window.history.pushState({},"", "?l="+lang+"&lat=" + lat + "&lon=" + lon + "&zoom=" + zoom);
});
var markers = L.markerClusterGroup({
    maxClusterRadius: function (zoom) {
        return (zoom <= 14) ? 40 : 1; // radius in pixels
    },
    // maxClusterRadius: 40,
    animateAddingMarkers: true
});
var list_markers = [];
if (placeid !== null && placeid != undefined && placeid !== "null" && placeid !== "none") {
  showRightCard(placeid);
  var id_to_popup = placeid;
}

$.get("/getCategories/lang/"+lang, function(data){
  categories = data["categories"];
  sorted_category_slugs = data["sorted_slugs"];
  printFiltersList();
  var social_currency_networks = [];
  var database_points = [];
  // get items data

      $.get("map_points/?l="+lang+"", function(data){
        allitems = JSON.parse(JSON.stringify(data.response));
        items = allitems;
        if(param_filters == undefined){
            list_markers = updateMarkers(id_to_popup);
            turnLoadingBar(false);
        }

        checkInitialFilters(param_filters);
      });

});

var sidebar = L.control.sidebar('sidebar').addTo(map);
